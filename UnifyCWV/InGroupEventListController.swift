//
//  InGroupEventListController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 9/8/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit

class InGroupEventListController: UITableViewController {
    
    var events: [Event] = []
    var passedEvents: [Key: Bool] = [:]
    var keys: [Key] = []
    var groupName: String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        incomeNewEvents()
    }
    
    func incomeNewEvents() {
        for eventIDValueAndKey in passedEvents {
            let key = eventIDValueAndKey.key
            keys.append(key)
        }
        
        if let events = Event.get() {
            for event in events {
                if keys.contains(event.key) {
                    self.events.append(event)
                }
            }
        }
        
        self.tableView.reloadData()
        Filter(disable: true)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let fullEventListVC = storyboard?.instantiateViewController(withIdentifier: "FullEventViewController") as? FullEventViewController {
            fullEventListVC.event = events[indexPath.row]
            fullEventListVC.groupName = self.groupName
            navigationController?.pushViewController(fullEventListVC, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let event = events[indexPath.row]
        let dateLabel = cell.viewWithTag(10) as! UILabel
        dateLabel.text = "\(event.startTime.makeStringFromHHmm().toTwelve()) | \(event.date.makeStringFromYMD())"
        let nameLabel = cell.viewWithTag(11) as! UILabel
        nameLabel.text = event.name
        let locationLabel = cell.viewWithTag(13) as! UILabel
        ///locationLabel.text = event.locationName ?? event.makeAddress(ofType: .Display) ?? "Location Unknown"
        
        return cell
    }
}
