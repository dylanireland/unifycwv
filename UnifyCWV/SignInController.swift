//
//  SignInController.swift
//  UnifyCWV
//
//  Created by Samuel Mckenney on 5/27/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import EGFormValidator
import SCLAlertView
import FirebaseAuth
import FirebaseDatabase

class SignInController: ValidatorViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    @IBOutlet weak var signInButton: UIButton!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var navItem: UINavigationItem!
    
    @IBOutlet weak var navBar: UINavigationBar!
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        navBar.titleTextAttributes = [NSFontAttributeName: UIFont().Default(withSize: 18), NSForegroundColorAttributeName: UIColor.white]

        navItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont().Default(withSize: 15)], for: .normal)
        
        self.addValidatorEmail(toControl: emailField, errorPlaceholder: emailLabel, errorMessage: "Email is not valid")
        
        self.addValidatorMandatory(toControl: emailField, errorPlaceholder: emailLabel, errorMessage: "This field is required")
        
        self.addValidatorMandatory(toControl: passwordField, errorPlaceholder: passwordLabel, errorMessage: "This field is required")
        
        emailField.delegate = self
        passwordField.delegate = self
        
        signInButton.addTarget(self, action: #selector(checkFields), for: .touchUpInside)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        emailField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if emailField.isFirstResponder {
            emailField.resignFirstResponder()
        } else if passwordField.isFirstResponder {
            passwordField.resignFirstResponder()
        }
    }
    
    func checkFields() {
        if self.validate() {
            signIn(email: emailField.text!, password: passwordField.text!)
        }
    }
    
    func signIn(email: String, password: String) {
        Filter(disable: false)
        Auth.auth().signIn(withEmail: email, password: password, completion: { user, error in
            
            guard error == nil else {
                self.Filter(disable: true)
                SCLAlertView().showError("Error", subTitle: error!.localizedDescription)
                if self.passwordField.isFirstResponder {
                    self.passwordField.resignFirstResponder()
                } else if self.emailField.isFirstResponder {
                    self.emailField.resignFirstResponder()
                }
                return
            }
            if let user = user {
                self.ref.child("users").child(user.uid).observeSingleEvent(of: .value, with: { snapshot in
                    if let value = snapshot.value as? [Key: AnyObject] {
                        let name = value["name"] as? String
                        let email = value["email"] as? String
                        let admin = value["admin"] as? Bool
                        let valid = value["valid"] as? Bool
                        let phone = value["phone"] as? String
                        let complete = value["complete"] as? Bool
                        let groups = value["groups"] as? [Key: Name]
                        let requests = value["requests"] as? [Key: Name]
                        
                        User(uid: user.uid, name: name, email: email, admin: admin, valid: valid, phone: phone, complete: complete, groups: groups, requests: requests).set()
                        
                        User.startObservingUserInfo()
                        
                        self.Filter(disable: true)
                        self.dismiss(animated: true, completion: nil)
                    }  else {
                        self.Filter(disable: true)
                        SCLAlertView().showError("Error", subTitle: "Your user ID no longer matches our database")
                    }
                })
            }
        })
    }
    
    @IBAction func signUpPressed() {
        segue()
    }
    
    func segue() {
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(containerView)
        NSLayoutConstraint.activate([
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            containerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            ])
        
        let controller = storyboard!.instantiateViewController(withIdentifier: "SignUpController")
        
        addChildViewController(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(controller.view)
        
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            controller.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            controller.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            controller.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
            ])
        
        controller.didMove(toParentViewController: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
            case emailField: passwordField.becomeFirstResponder()
            case passwordField: passwordField.resignFirstResponder(); checkFields()
            default: break
        }
        return true
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func keyboardWasShown(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.bottomConstraint.constant = keyboardFrame.size.height
        })
    }
    
    func keyboardWasHidden(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.bottomConstraint.constant = 0
        })
    }
}
