//
//  RearViewController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 8/22/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import FirebaseAuth
import SCLAlertView

class RearViewController: UIViewController {
    
    @IBOutlet weak var myInfo: UIButton!
    @IBOutlet weak var signButton: UIButton!
    @IBOutlet weak var admin: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Auth.auth().currentUser == nil {
            signButton.setTitle("Sign In", for: .normal)
            myInfo.isHidden = true
        } else {
            signButton.setTitle("Sign Out", for: .normal)
            myInfo.isHidden = false
        }
        if let adminity = User.getCurrentUser()?.admin, adminity {
            admin.isHidden = false
        } else {
            admin.isHidden = true
        }
    }
    
    @IBAction func signAction() {
        if Auth.auth().currentUser == nil {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "signInSegue", sender: self)
            }
        } else {
            do { try
                Auth.auth().signOut()
                User.signOut()
                signButton.setTitle("Sign In", for: .normal)
                myInfo.isHidden = true
                admin.isHidden = true
                SCLAlertView().showSuccess("We'll Miss You!", subTitle: "You have been successfully signed out")
            } catch {
                SCLAlertView().showError("Whoops", subTitle: "There was an error signing you out, please try again")
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
