//
//  AboutControllerViewController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 8/14/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import MessageUI
import FirebaseAuth

class AboutController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var nav: UINavigationBar!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var sendUsFeedbackButton: UIButton!
    @IBOutlet weak var explainationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nav.titleTextAttributes = [NSFontAttributeName: UIFont().Default(withSize: 18), NSForegroundColorAttributeName: UIColor.white]
        navItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont().Default(withSize: 15)], for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if MFMailComposeViewController.canSendMail() {
            sendUsFeedbackButton.isEnabled = true
            explainationLabel.isHidden = true
            sendUsFeedbackButton.setTitleColor(UIColor.Default, for: .normal)
        } else {
            sendUsFeedbackButton.isEnabled = false
            explainationLabel.isHidden = false
            sendUsFeedbackButton.setTitleColor(.lightGray, for: .normal)
        }
    }

    @IBAction func sendUsFeedback() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        let email = System.getAdminEmail()
        
        var username = "Unknown"
        var useremail = "Unknown"
        var userID = "Unknown"
        var userphone = "Unknown"
        var uservalid = "Unknown"
        
        if let name = User.getCurrentUser()?.name {
            username = name
        }
        
        if let email = Auth.auth().currentUser?.email {
            useremail = email
        }
        
        if let uid = Auth.auth().currentUser?.uid {
            userID = uid
        }
        
        if let phone = User.getCurrentUser()?.phone {
            userphone = phone
        }
        
        if let valid = User.getCurrentUser()?.valid {
            uservalid = valid.description
        }
        
        composeVC.setToRecipients([email])
        composeVC.setSubject("UnifyCWV Feedback")
        composeVC.setMessageBody("Name: \(username)\nEmail: \(useremail)\nUserID: \(userID)\nPhone: \(userphone)\nValid: \(uservalid)\n\n", isHTML: false)
        
        self.present(composeVC, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
