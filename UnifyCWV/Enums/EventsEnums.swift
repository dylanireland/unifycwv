//
//  Events.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation



enum EventHelp: String {
    case name = "The title of your event"
    case contact_person = "The individual to be contacted for any questions"
    case phone_number = "The phone number for contacting the owner of the event"
    case website = "The website for your event"
    case facebook_id = "The ID of your event on Facebook. JUST the ID, not \"https://facebook.com/event/\""
    case description = "Description of your event"
    case email = "The email address used to contact the event owner"
    case street_address_1 = "The first street address of your event (123 Main Street)"
    case street_address_2 = "The first street address of your event (Floor 4)"
    case street_address_3 = "The first street address of your event (Apt 3)"
    case city = "The city in which your event resides"
    case state = "The state in which your event resides"
    case zip = "The zip code in which your event resides"
}
