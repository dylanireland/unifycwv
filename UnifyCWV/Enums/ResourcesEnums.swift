//
//  ResourcesEnums.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation

enum ResourceHelp: String {
    case name = "The name of your resource"
    case contact_person = "The individual to be contacted for any questions"
    case phone_number = "The phone number for contacting the owner of the resource"
    case website = "The website for your resource"
    case facebook_id = "The ID of your resource on Facebook. JUST the ID, not \"https://facebook.com/event/\""
    case twitter_handle = "The handle of your resource's twitter account"
    case description = "Description of your resource"
    case email = "The email address used to contact the resource owner"
    case street_address_1 = "The first street address of your resource (123 Main Street)"
    case street_address_2 = "The first street address of your resource (Floor 4)"
    case street_address_3 = "The first street address of your resource (Apt 3)"
    case city = "The city in which your resource resides"
    case state = "The state in which your resource resides"
    case zip = "The zip code in which your resource resides"
}
