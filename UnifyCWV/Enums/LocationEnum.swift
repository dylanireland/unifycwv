//
//  Location.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation

enum LocationEnum: Int {
    case display = 0
    case full = 1
}
