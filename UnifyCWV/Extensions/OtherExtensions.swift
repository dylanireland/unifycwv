//
//  Other.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    static let filter: UIView = UIView()
    static let filterActivity: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func Filter(disable: Bool) {
        if disable {
            if UIViewController.filter.superview != nil {
                self.view.isUserInteractionEnabled = true
                UIView.animate(withDuration: 0.7, animations: {
                    UIViewController.filter.alpha = 0.0
                }, completion: { _ in
                    UIViewController.filter.removeFromSuperview()
                })
                if UIViewController.filterActivity.superview != nil {
                    UIView.animate(withDuration: 0.7, animations: {
                        UIViewController.filterActivity.alpha = 0.0
                    }, completion: { _ in
                        UIViewController.filterActivity.removeFromSuperview()
                    })
                }
            }
        } else {
            if UIViewController.filter.superview == nil {
                self.view.isUserInteractionEnabled = false
                let window = UIApplication.shared.keyWindow!
                UIViewController.filter.frame = CGRect(x: self.view.frame.minX, y: self.view.frame.minY, width: self.view.frame.width + (self.view.frame.width / 8), height: self.view.frame.height + (self.view.frame.height / 8))
                UIViewController.filter.backgroundColor = UIColor(hex: 0x333333)
                UIViewController.filter.alpha = 0.0
                window.addSubview(UIViewController.filter)
                UIView.animate(withDuration: 0.7, animations: {
                    UIViewController.filter.alpha = 0.6
                })
                if UIViewController.filterActivity.superview == nil {
                    UIViewController.filterActivity.startAnimating()
                    UIViewController.filterActivity.activityIndicatorViewStyle = .whiteLarge
                    UIViewController.filterActivity.frame = CGRect(x: self.view.center.x - 30, y: self.view.center.y - 30, width: 60, height: 60)
                    UIViewController.filterActivity.alpha = 0.0
                    window.addSubview(UIViewController.filterActivity)
                    UIView.animate(withDuration: 0.7, animations: {
                        UIViewController.filterActivity.alpha = 1.0
                    })
                }
            }
        }
    }
}

extension UIViewController {
    func topMostViewController() -> UIViewController {
        if self.presentedViewController == nil {
            return self
        }
        if let navigation = self.presentedViewController as? UINavigationController {
            return navigation.visibleViewController!.topMostViewController()
        }
        if let tab = self.presentedViewController as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController!.topMostViewController()
    }
}

extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}

extension UITextField {
    func isNilOrEmpty() -> Bool {
        let value = self.text
        if value == "" || value == nil {
            return true
        } else {
            return false
        }
    }
}

extension Int {
    func monthToMonth() -> String? {
        switch self {
        case 1:
            return "January"
        case 2:
            return "February"
        case 3:
            return "March"
        case 4:
            return "April"
        case 5:
            return "May"
        case 6:
            return "June"
        case 7:
            return "July"
        case 8:
            return "August"
        case 9:
            return "September"
        case 10:
            return "October"
        case 11:
            return "November"
        case 12:
            return "December"
        default: return nil
        }
    }
    
    func daySuffix() -> String {
        switch self {
        case 1:
            return "st"
        case 2:
            return "nd"
        case 3:
            return "rd"
        case 4:
            return "th"
        case 5:
            return "th"
        case 6:
            return "th"
        case 7:
            return "th"
        case 8:
            return "th"
        case 9:
            return "th"
        case 10:
            return "th"
        case 11:
            return "th"
        case 12:
            return "th"
        case 13:
            return "th"
        case 14:
            return "th"
        case 15:
            return "th"
        case 16:
            return "th"
        case 17:
            return "th"
        case 18:
            return "th"
        case 19:
            return "th"
        case 20:
            return "th"
        case 21:
            return "st"
        case 22:
            return "nd"
        case 23:
            return "rd"
        case 24:
            return "th"
        case 25:
            return "th"
        case 26:
            return "th"
        case 27:
            return "th"
        case 28:
            return "th"
        case 29:
            return "th"
        case 30:
            return "th"
        case 31:
            return "st"
        default: return ""
        }
    }
    
    func makeRandomString() -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        var randomString = ""
        
        for _ in 0 ..< self {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
}

extension Array where Element: Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}
