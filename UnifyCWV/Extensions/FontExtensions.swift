//
//  FontExtensions.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation
import UIKit.UIFont

extension UIFont {
    func Default(withSize size: CGFloat) -> UIFont {
        return UIFont(name: "ElenarLove-Regular", size: size)!
    }
}
