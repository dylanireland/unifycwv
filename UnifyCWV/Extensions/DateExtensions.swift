//
//  DateExtensions.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/19/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation

extension Date {
    func makeStringFromHHmm() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: self)
    }
    
    func makeStringFromYMD() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        var ymd = dateFormatter.string(from: self).description.components(separatedBy: "/")
        let year = ymd.first!
        let mo = Int(ymd[1])!
        let day = Int(ymd.last!)!
        
        let month = mo.monthToMonth()
        let suffix = day.daySuffix()
        
        return "\(month!) \(day)\(suffix), \(year)"
    }
    
    func makeShorthandStringFromYMD() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        var ymd = dateFormatter.string(from: self).description.components(separatedBy: "/")
        let year = ymd.first!
        let month = Int(ymd[1])!
        let day = Int(ymd.last!)!
        return "\(month)/\(day)/\(year)"
    }
    
    func makeEuropeanShorthandStringFromYMD() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        return dateFormatter.string(from: self)
    }
    
    func combineWith(time: Date) -> Date? {
        let calendar = Calendar.current
        
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: self)
        let timeComponents = calendar.dateComponents([.hour, .minute], from: time)
        guard let year = dateComponents.year, let month = dateComponents.month, let day = dateComponents.day, let hour = timeComponents.hour, let minute = timeComponents.minute else {
            return nil
        }
        
        var mergedComponments = DateComponents()
        mergedComponments.year = year
        mergedComponments.month = month
        mergedComponments.day = day
        mergedComponments.hour = hour
        mergedComponments.minute = minute
        
        return calendar.date(from: mergedComponments)
    }
    
    func provideSearchableText() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd yyyy"
        return dateFormatter.string(from: self)
    }
    
    func equate() -> [String]? {
        guard let events = Event.get() else {
            return nil
        }
        
        var Return: [String]? = nil
        let calendar = Calendar.current
        for event in events {
            let components = calendar.dateComponents([.day], from: event.date, to: event.endDate)
            if let days = components.day {
                for index in 0..<days + 1 {
                    let abstractDate = Calendar.current.date(byAdding: .day, value: index, to: event.date)
                    if self == abstractDate {
                        if Return == nil {
                            Return = []
                        }
                        Return!.append(event.key)
                    }
                }
            }
        }
        return Return != nil ? Return!.removeDuplicates(): nil
    }
}
