//
//  String.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation

extension String {
    func toTwelve() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: date!)
    }
    
    var strictlyNumbersAllowed: Bool {
        let characterSet = CharacterSet(charactersIn: "1234567890").inverted
        let range = (self as NSString).rangeOfCharacter(from: characterSet)
        return range.location != NSNotFound
    }
    
    func makeDateFromYMD() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let date = dateFormatter.date(from: self)
        return date
    }
    
    func makeDateFromHHmm() -> Date? {
        let vals = self.description.components(separatedBy: ":")
        let hour = vals.first!
        let minute = vals[1]
        
        let time = "\(hour):\(minute)"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.date(from: time)
        
        return date
    }
    
    func makeStringFromHHmm() -> String {
        let vals = self.description.components(separatedBy: ":")
        let hour = vals.first!
        let minute = vals[1]
        
        let time = "\(hour):\(minute)".toTwelve()
        
        return time
    }
}
