//
//  TabBarController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 8/13/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import SWRevealViewController
import SCLAlertView

class TabBarController: UITabBarController {
    @IBOutlet weak var open: UIBarButtonItem!
    var rightBarButtonItem = UIBarButtonItem()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.red
        self.navigationController?.navigationBar.tintColor = UIColor.Default
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont().Default(withSize: 18)]
        
        rightBarButtonItem = UIBarButtonItem(title: "New", style: .plain, target: self, action: #selector(rightBarButtonAction))
        navigationItem.rightBarButtonItem = rightBarButtonItem
        
        open.target = self.revealViewController()
        open.action = #selector(SWRevealViewController.revealToggle(_:))
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        NotificationCenter.default.addObserver(self, selector: #selector(whichController), name: NSNotification.Name("tabBarViewChanged"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        /*TEST do {
            try NSExceptionHandling.catchException {
                print(Event.get())
            }
        } catch {
            print("ERROR: " + error.localizedDescription)
        }*/
        do {
            try System.checkDatabaseVersion()
        } catch System.DatabaseVersionErrors.maintenance {
            showDatabaseVersion(withError: System.DatabaseVersionErrors.maintenance)
        } catch System.DatabaseVersionErrors.needsUpdated {
            showDatabaseVersion(withError: System.DatabaseVersionErrors.needsUpdated)
        } catch {
            showDatabaseVersion(withError: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !Terms().get() {
            showTerms()
        }
    }
    
    func whichController(sender: Notification) {
        if sender.userInfo?["key"] as! Int == 3, User.getCurrentUser()?.valid == true {
            rightBarButtonItem.tintColor = UIColor(red: 23, green: 70, blue: 186)
            rightBarButtonItem.isEnabled = true
        } else {
            rightBarButtonItem.tintColor = UIColor.clear
            rightBarButtonItem.isEnabled = false
        }
    }
    
    func rightBarButtonAction() {
        let AddOrgVC = storyboard?.instantiateViewController(withIdentifier: "AddOrganizationController") as! AddOrganizationController
        navigationController?.pushViewController(AddOrgVC, animated: true)
    }
    
    func showTerms() {
        let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
        let alert = SCLAlertView(appearance: appearance)
        alert.addButton("Agree", action: {
            Terms().validate()
        })
        alert.addButton("Disagree", action: {
            self.showNoticeOfExit()
        })
        alert.addButton("View Terms", action: {
            guard let url = URL(string: "https://docs.google.com/document/d/1GPy-UEZdkJW4TCBlvmyRPUeyWuOctnTbE04MCGo2mHI/edit") else {
                alert.dismiss(animated: true)
                self.showTerms()
                return
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        })
        alert.showInfo("Terms and Conditions", subTitle: Terms.TermsSubs.alertSubtitle.rawValue)
    }
    
    func showNoticeOfExit() {
        let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
        let alert = SCLAlertView(appearance: appearance)
        alert.addButton("Close", action: {
            exit(0)
        })
        alert.showInfo("Terms and Conditions", subTitle: Terms.TermsSubs.exitSubtitle.rawValue)
    }
    
    func showDatabaseVersion(withError error: System.DatabaseVersionErrors?) {
        guard let error = error else {
            postDefaultError()
            return
        }
        switch error {
        case System.DatabaseVersionErrors.maintenance:
            postDefaultError()
        case System.DatabaseVersionErrors.needsUpdated:
            let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Go to AppStore") {
                let url = NSURL(string: "https://itunes.apple.com/us/app/unifycwv/id1294150738?ls=1&mt=8")! as URL
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            alertView.addButton("Close App", action: {
                exit(0)
            })
            alertView.showInfo("Info", subTitle: "Please update UnifyCWV for new great features, thanks")
        }
    }
    
    func postDefaultError() {
        let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("Close App", action: {
            exit(0)
        })
        alertView.showInfo("Info", subTitle: "Database is under maintenance, please standby while we resolve this issue")
    }
}
