//
//  Groups.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation

class Group: NSObject, NSCoding {
    var name: String
    var desc: String
    var contact: String
    var position: String
    var phone: String?
    var email: String?
    var website: String?
    var facebook: String?
    var date: Date
    var startTime: Date
    var endDate: Date
    var endTime: Date
    var locationName: String?
    var location: Location?
    
    init(name: String, description: String, contact: String, position: String, phone: String?, email: String?, website: String?, facebook: String?, date: Date, startTime: Date, endDate: Date, endTime: Date, locationName: String, location: Location?) {
        self.name = name
        self.desc = description
        self.contact = contact
        self.phone = phone
        self.email = email
        self.website = website
        self.facebook = facebook
        self.date = date
        self.startTime = startTime
        self.endDate = endDate
        self.endTime = endTime
        self.locationName = locationName
        self.location = location
    }
    
    required init(coder decoder: NSCoder) {
        self.name = decoder.decodeObject(forKey: NSCoderKeys.name.rawValue) as! String
        self.desc = decoder.decodeObject(forKey: NSCoderKeys.description.rawValue) as! String
        self.contact = decoder.decodeObject(forKey: NSCoderKeys.contact.rawValue) as! String
        self.position = decoder.decodeObject(forKey: NSCoderKeys.position.rawValue) as! String
        self.phone = decoder.decodeObject(forKey: NSCoderKeys.phone.rawValue) as? String
        self.email = decoder.decodeObject(forKey: NSCoderKeys.email.rawValue) as? String
        self.website = decoder.decodeObject(forKey: NSCoderKeys.website.rawValue) as? String
        self.facebook = decoder.decodeObject(forKey: NSCoderKeys.facebook.rawValue) as? String
        self.date = decoder.decodeObject(forKey: NSCoderKeys.date.rawValue) as! Date
        self.startTime = decoder.decodeObject(forKey: NSCoderKeys.startTime.rawValue) as! Date
        self.endDate = decoder.decodeObject(forKey: NSCoderKeys.endDate.rawValue) as! Date
        self.endTime = decoder.decodeObject(forKey: NSCoderKeys.endTime.rawValue) as! Date
        self.locationName = decoder.decodeObject(forKey: NSCoderKeys.locationName.rawValue) as? String
        self.location = decoder.decodeObject(forKey: NSCoderKeys.location.rawValue) as? Location
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(name, forKey: NSCoderKeys.name.rawValue)
        coder.encode(desc, forKey: NSCoderKeys.description.rawValue)
        coder.encode(contact, forKey: NSCoderKeys.contact.rawValue)
        coder.encode(position, forKey: NSCoderKeys.contact.rawValue)
        coder.encode(phone, forKey: NSCoderKeys.phone.rawValue)
        coder.encode(email, forKey: NSCoderKeys.email.rawValue)
        coder.encode(website, forKey: NSCoderKeys.website.rawValue)
        coder.encode(facebook, forKey: NSCoderKeys.facebook.rawValue)
        coder.encode(date, forKey: NSCoderKeys.date.rawValue)
        coder.encode(startTime, forKey: NSCoderKeys.startTime.rawValue)
        coder.encode(endDate, forKey: NSCoderKeys.endDate.rawValue)
        coder.encode(endTime, forKey: NSCoderKeys.endTime.rawValue)
        coder.encode(locationName, forKey: NSCoderKeys.locationName.rawValue)
        coder.encode(location, forKey: NSCoderKeys.location.rawValue)
    }
    
    enum NSCoderKeys: String {
        case name = "event_name"
        case description = "event_description"
        case contact = "event_contact"
        case position = "event_position"
        case phone = "event_phone"
        case email = "event_email"
        case website = "event_website"
        case facebook = "event_facebook"
        case date = "event_date"
        case startTime = "event_startTime"
        case endDate = "event_endDate"
        case endTime = "event_endTime"
        case locationName = "event_locationName"
        case location = "event_location"
    }
}

