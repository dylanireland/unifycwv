//
//  GroupRequests.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/20/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation
import FirebaseDatabase

class GroupRequest: NSObject, NSCoding {
    var groupID: Key
    var userID: Key
    var name: Name
    
    init(groupID: Key, userID: Key, name: Name) {
        self.groupID = groupID
        self.userID = userID
        self.name = name
    }
    
    required init(coder decoder: NSCoder) {
        self.groupID = decoder.decodeObject(forKey: NSCoderKeys.groupID.rawValue) as! Key
        self.userID = decoder.decodeObject(forKey: NSCoderKeys.userID.rawValue) as! Key
        self.name = decoder.decodeObject(forKey: NSCoderKeys.name.rawValue) as! Name
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(groupID, forKey: NSCoderKeys.groupID.rawValue)
        coder.encode(userID, forKey: NSCoderKeys.userID.rawValue)
        coder.encode(name, forKey: NSCoderKeys.name.rawValue)
    }
    
    enum NSCoderKeys: String {
        case groupID = "group_request_groupID"
        case userID = "group_request_userID"
        case name = "group_request_name"
    }
    
    enum GetterSetterNSCoderKeys: String {
        case key = "GROUP_REQUEST_NSCODER_KEY"
    }
    
    class func set(listOf groupRequests: [GroupRequest]) {
        let encoded = NSKeyedArchiver.archivedData(withRootObject: groupRequests)
        defaults.set(encoded, forKey: GetterSetterNSCoderKeys.key.rawValue)
    }
    
    class func get() -> [GroupRequest]? {
        if let data = defaults.data(forKey: GetterSetterNSCoderKeys.key.rawValue) {
            let groupRequests = NSKeyedUnarchiver.unarchiveObject(with: data) as? [GroupRequest]
            return groupRequests
        } else {
            return nil
        }
    }
    
    class func observe() {
        let ref = Database.database().reference().child("group-requests")
        ref.observe(.value, with: { snapshot in
            guard let snapshot = snapshot.value as? [Key: AnyObject] else {
                return
            }
            var group_requests: [GroupRequest] = []
            for group_request in snapshot {
                if let value = group_request.value as? [Key: AnyObject] {
                    for user in value {
                        let groupID = group_request.key
                        let uid = user.key
                        let name = user.value as? Key
                        if let name = name {
                            group_requests.append(GroupRequest(groupID: groupID, userID: uid, name: name))
                        }
                    }
                }
            }
            set(listOf: group_requests)
            NotificationCenter.default.post(name: NSNotification.Name("incomeNewData"), object: nil)
        })
    }
}
