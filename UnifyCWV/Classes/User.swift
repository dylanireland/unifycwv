//
//  CurrentUser.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class User: NSObject, NSCoding {
    var uid: String
    var name: String?
    var email: String?
    var admin: Bool?
    var valid: Bool?
    var phone: String?
    var complete: Bool?
    var groups: [Key: Name]?
    var requests: [Key: Name]?
    static var userInfoReference = Database.database().reference().child("users")
    
    init(uid: String, name: String?, email: String?, admin: Bool?, valid: Bool?, phone: String?, complete: Bool?, groups: [Key: Name]?, requests: [Key: Name]?) {
        self.uid = uid
        self.name = name
        self.email = email
        self.admin = admin
        self.valid = valid
        self.phone = phone
        self.complete = complete
        self.groups = groups
        self.requests = requests
    }
    
    required init(coder decoder: NSCoder) {
        self.uid = decoder.decodeObject(forKey: NSCoderKeys.uid.rawValue) as! String
        self.name = decoder.decodeObject(forKey: NSCoderKeys.name.rawValue) as? String
        self.email = decoder.decodeObject(forKey: NSCoderKeys.email.rawValue) as? String
        if let admin = decoder.decodeObject(forKey: NSCoderKeys.admin.rawValue) as? Bool {
            self.admin = admin
        } else {
            self.admin = nil
        }
        if let valid = decoder.decodeObject(forKey: NSCoderKeys.valid.rawValue) as? Bool {
            self.valid = valid
        } else {
            self.valid = nil
        }
        self.phone = decoder.decodeObject(forKey: NSCoderKeys.phone.rawValue) as? String
        if let complete = decoder.decodeObject(forKey: NSCoderKeys.complete.rawValue) as? Bool {
            self.complete = complete
        } else {
            self.complete = nil
        }
        self.groups = decoder.decodeObject(forKey: NSCoderKeys.groups.rawValue) as? [Key: Name]
        self.requests = decoder.decodeObject(forKey: NSCoderKeys.requests.rawValue) as? [Key: Name]
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(uid, forKey: NSCoderKeys.uid.rawValue)
        coder.encode(name, forKey: NSCoderKeys.name.rawValue)
        coder.encode(email, forKey: NSCoderKeys.email.rawValue)
        coder.encode(admin as AnyObject, forKey: NSCoderKeys.admin.rawValue)
        coder.encode(valid as AnyObject, forKey: NSCoderKeys.valid.rawValue)
        coder.encode(phone, forKey: NSCoderKeys.phone.rawValue)
        coder.encode(complete as AnyObject, forKey: NSCoderKeys.complete.rawValue)
        coder.encode(groups, forKey: NSCoderKeys.groups.rawValue)
        coder.encode(requests, forKey: NSCoderKeys.requests.rawValue)
    }
    
    enum NSCoderKeys: String {
        case uid = "current_user_uid"
        case name = "current_user_name"
        case email = "current_user_email"
        case admin = "current_user_admin"
        case valid = "current_user_valid"
        case phone = "current_user_phone"
        case complete = "current_user_complete"
        case groups = "current_user_groups"
        case requests = "current_user_requests"
    }
    
    enum GetterSetterNSCoderKeys: String {
        case current = "CURRENT_USER_NSCODER_KEY"
        case other = "OTHER_USER_NSCODER_KEY"
    }
    
    class func set(users: [User]) {
        let encoded = NSKeyedArchiver.archivedData(withRootObject: users)
        defaults.set(encoded, forKey: GetterSetterNSCoderKeys.other.rawValue)
    }
    
    func set() {
        let encoded = NSKeyedArchiver.archivedData(withRootObject: self)
        defaults.set(encoded, forKey: GetterSetterNSCoderKeys.current.rawValue)
    }
    
    func update(name: String? = nil, email: String? = nil, admin: Bool? = nil, valid: Bool? = nil, phone: String? = nil, complete: Bool? = nil, groups: [Key: Name]? = nil, requests: [Key: Name]? = nil) {
        self.name = name ?? self.name
        self.email = email ?? self.email
        self.admin = admin ?? self.admin
        self.valid = valid ?? self.valid
        self.phone = phone ?? self.phone
        self.complete = complete ?? self.complete
        self.groups = groups ?? self.groups
        self.requests = requests ?? self.requests
        self.set()
    }
    
    class func getUsers() -> [User]? {
        if let data = defaults.data(forKey: GetterSetterNSCoderKeys.other.rawValue) {
            let users = NSKeyedUnarchiver.unarchiveObject(with: data) as? [User]
            return users
        } else {
            return nil
        }
    }
    
    class func getCurrentUser() -> User? {
        if let data = defaults.data(forKey: GetterSetterNSCoderKeys.current.rawValue) {
            let user = NSKeyedUnarchiver.unarchiveObject(with: data) as? User
            return user
        } else {
            return nil
        }
    }
    
    class func signOut() {
        defaults.set(nil, forKey: GetterSetterNSCoderKeys.current.rawValue)
    }
    
    class func startObservingUserInfo() {
        guard let user = Auth.auth().currentUser else {
            return
        }
        userInfoReference.child(user.uid).observe(.value, with: { snapshot in
            if let value = snapshot.value as? [Key: AnyObject] {
                let name = value["fullname"] as? String
                let email = value["email"] as? String
                let admin = value["admin"] as? Bool
                let valid = value["valid"] as? Bool
                let phone = value["phone"] as? String
                let complete = value["complete"] as? Bool
                let groups = value["groups"] as? [Key: Name]
                let requests = value["requests"] as? [Key: Name]
                User.getCurrentUser()?.update(name: name, email: email, admin: admin, valid: valid, phone: phone, complete: complete, groups: groups, requests: requests)
            }
        })
    }
    
    class func startObservingUsers() {
        userInfoReference.observe(.value, with: { snapshot in
            if let snapshot = snapshot.value as? [Key: AnyObject] {
                var users = [User]()
                for user in snapshot {
                    if let value = user.value as? [Key: AnyObject] {
                        let name = value["fullname"] as? String
                        let email = value["email"] as? String
                        let admin = value["admin"] as? Bool
                        let valid = value["valid"] as? Bool
                        let phone = value["phone"] as? String
                        let complete = value["complete"] as? Bool
                        let groups = value["groups"] as? [Key: Name]
                        let requests = value["requests"] as? [Key: Name]
                        let user = User(uid: user.key, name: name, email: email, admin: admin, valid: valid, phone: phone, complete: complete, groups: groups, requests: requests)
                        users.append(user)
                    }
                }
                set(users: users)
            }
        })
    }
    
    class func stopObservingUserInfo() {
        userInfoReference.removeAllObservers()
    }
    
    class func separateValids(from users: [User]) -> (valids: [User], invalids: [User]) {
        var valids: [User] = [User]()
        var invalids: [User] = [User]()
        
        for user in users {
            if let valid = user.valid {
                if valid {
                    valids.append(user)
                } else {
                    invalids.append(user)
                }
            }
        }
        return (valids, invalids)
    }
}
