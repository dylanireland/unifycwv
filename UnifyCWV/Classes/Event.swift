//
//  Event.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation
import FirebaseDatabase

class Event: NSObject, NSCoding {
    var key: Key
    var name: String
    var desc: String
    var contact: String?
    var phone: String?
    var email: String?
    var website: String?
    var facebook: String?
    var date: Date
    var startTime: Date
    var endDate: Date
    var endTime: Date
    var locationName: String?
    var location: Location?
    var valid: Bool
    var groupId: String
    var groupName: String?
    
    init(key: Key, name: String, description: String, contact: String?, phone: String?, email: String?, website: String?, facebook: String?, date: Date, startTime: Date, endDate: Date, endTime: Date, locationName: String?, location: Location?, valid: Bool, groupId: String) {
        self.key = key
        self.name = name
        self.desc = description
        self.contact = contact
        self.phone = phone
        self.email = email
        self.website = website
        self.facebook = facebook
        self.date = date
        self.startTime = startTime
        self.endDate = endDate
        self.endTime = endTime
        self.locationName = locationName
        self.location = location
        self.valid = valid
        self.groupId = groupId
        self.groupName = nil
        if let groups = Group.get() {
            for group in groups {
                if self.groupId == group.key {
                    self.groupName = group.name
                    break
                }
            }
        }
    }
    
    required init(coder decoder: NSCoder) {
        self.key = decoder.decodeObject(forKey: NSCoderKeys.key.rawValue) as! Key
        self.name = decoder.decodeObject(forKey: NSCoderKeys.name.rawValue) as!String
        self.desc = decoder.decodeObject(forKey: NSCoderKeys.description.rawValue) as!String
        self.contact = decoder.decodeObject(forKey: NSCoderKeys.contact.rawValue) as? String
        self.phone = decoder.decodeObject(forKey: NSCoderKeys.phone.rawValue) as? String
        self.email = decoder.decodeObject(forKey: NSCoderKeys.email.rawValue) as? String
        self.website = decoder.decodeObject(forKey: NSCoderKeys.website.rawValue) as? String
        self.facebook = decoder.decodeObject(forKey: NSCoderKeys.facebook.rawValue) as? String
        self.date = decoder.decodeObject(forKey: NSCoderKeys.date.rawValue) as! Date
        self.startTime = decoder.decodeObject(forKey:NSCoderKeys.startTime.rawValue) as! Date
        self.endDate = decoder.decodeObject(forKey: NSCoderKeys.endDate.rawValue) as! Date
        self.endTime = decoder.decodeObject(forKey: NSCoderKeys.endTime.rawValue) as! Date
        self.locationName = decoder.decodeObject(forKey: NSCoderKeys.locationName.rawValue) as? String
        self.location = decoder.decodeObject(forKey: NSCoderKeys.location.rawValue) as? Location
        self.valid = decoder.decodeBool(forKey: NSCoderKeys.valid.rawValue)
        self.groupId = decoder.decodeObject(forKey: NSCoderKeys.groupId.rawValue) as! String
        self.groupName = decoder.decodeObject(forKey: NSCoderKeys.groupName.rawValue) as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(key, forKey: NSCoderKeys.key.rawValue)
        coder.encode(name, forKey: NSCoderKeys.name.rawValue)
        coder.encode(desc, forKey: NSCoderKeys.description.rawValue)
        coder.encode(contact, forKey: NSCoderKeys.contact.rawValue)
        coder.encode(phone, forKey: NSCoderKeys.phone.rawValue)
        coder.encode(email, forKey: NSCoderKeys.email.rawValue)
        coder.encode(website, forKey: NSCoderKeys.website.rawValue)
        coder.encode(facebook, forKey: NSCoderKeys.facebook.rawValue)
        coder.encode(date, forKey: NSCoderKeys.date.rawValue)
        coder.encode(startTime, forKey: NSCoderKeys.startTime.rawValue)
        coder.encode(endDate, forKey: NSCoderKeys.endDate.rawValue)
        coder.encode(endTime, forKey: NSCoderKeys.endTime.rawValue)
        coder.encode(locationName, forKey: NSCoderKeys.locationName.rawValue)
        coder.encode(valid, forKey: NSCoderKeys.valid.rawValue)
        coder.encode(groupId, forKey: NSCoderKeys.groupId.rawValue)
        coder.encode(location, forKey: NSCoderKeys.location.rawValue)
        coder.encode(groupName, forKey: NSCoderKeys.groupName.rawValue)
    }
    
    enum NSCoderKeys: String {
        case key = "event_key"
        case name = "event_name"
        case description = "event_description"
        case contact = "event_contact"
        case phone = "event_phone"
        case email = "event_email"
        case website = "event_website"
        case facebook = "event_facebook"
        case date = "event_date"
        case startTime = "event_startTime"
        case endDate = "event_endDate"
        case endTime = "event_endTime"
        case locationName = "event_locationName"
        case location = "event_location"
        case valid = "event_valid"
        case groupId = "event_groupId"
        case groupName = "event_groupName"
    }
    
    enum GetterSetterNSCoderKeys: String {
        case key = "EVENTS_NSCODER_KEY"
    }
    
    class func set(events: [Event]) {
        let encoded = NSKeyedArchiver.archivedData(withRootObject: events)
        defaults.set(encoded, forKey: GetterSetterNSCoderKeys.key.rawValue)
    }
    
    class func get() -> [Event]? {
        if let data = defaults.data(forKey: GetterSetterNSCoderKeys.key.rawValue) {
            let events = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Event]
            return events
        } else {
            return nil
        }
    }
    
    class func updateGroupNames(using groups: [Group]) {
        if let data = defaults.data(forKey: GetterSetterNSCoderKeys.key.rawValue), var events = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Event] {
            for group in groups {
                for index in 0..<events.count {
                    if events[index].groupId == group.key {
                        events[index].groupName = group.name
                    }
                }
            }
            set(events: events)
        }
    }
    
    class func observe() {
        let ref = Database.database().reference().child("events")
        ref.observe(.value, with: { snapshot in
            if let snapshot = snapshot.value as? [Key: AnyObject] {
                var events: [Event] = []
                for event in snapshot {
                    if let value = event.value as? [Key: AnyObject] {
                        let key = event.key
                        let name = value["name"] as? String
                        let description = value["description"] as? String
                        let contact = value["contact"] as? String
                        let phone = value["phone"] as? String
                        let email = value["email"] as? String
                        let website = value["website"] as? String
                        let facebook = value["facebook"] as? String
                        let date = (value["date"] as? String)?.makeDateFromYMD()
                        let startTime = (value["startTime"] as? String)?.makeDateFromHHmm()
                        let endDate = (value["endDate"] as? String)?.makeDateFromYMD()
                        let endTime = (value["endTime"] as? String)?.makeDateFromHHmm()
                        let locationName = value["locationName"] as? String
                        let valid = value["valid"] as? Bool
                        let groupId = value["groupId"] as? String
                        
                        var eventLocation: Location?
                        if let location = value["location"] as? [Key: AnyObject], let streetAddress1 = location["streetAddress1"] as? String, let city = location["city"] as? String, let state = location["state"] as? String, let zip = location["zip"] as? String {
                            let streetAddress2 = location["streetAddress2"] as? String
                            let streetAddress3 = location["streetAddress3"] as? String
                            let latitude = location["latitude"] as? Double
                            let longitude = location["longitude"] as? Double
                            let loc = Location(streetAddress1: streetAddress1, streetAddress2: streetAddress2, streetAddress3: streetAddress3, city: city, state: state, zip: zip, latitude: latitude, longitude: longitude)
                            eventLocation = loc
                        }
                        if let name = name, let description = description, let date = date, let startTime = startTime, let endTime = endTime, let valid = valid, let groupId = groupId {
                            events.append(Event(key: key, name: name, description: description, contact: contact, phone: phone, email: email, website: website, facebook: facebook, date: date, startTime: startTime, endDate: endDate ?? date, endTime: endTime, locationName: locationName, location: eventLocation, valid: valid, groupId: groupId))
                        }
                    }
                }
                set(events: events)
                NotificationCenter.default.post(name: NSNotification.Name("incomeNewData"), object: nil)
            }
        })
    }
}
