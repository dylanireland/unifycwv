//
//  Location.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation
import CoreLocation

class Location: NSObject, NSCoding {
    var streetAddress1: String
    var streetAddress2: String?
    var streetAddress3: String?
    var city: String
    var state: String
    var zip: String
    var latitude: CLLocationDegrees?
    var longitude: CLLocationDegrees?
    
    init(streetAddress1: String, streetAddress2: String?, streetAddress3: String?, city: String, state: String, zip: String, latitude: CLLocationDegrees?, longitude: CLLocationDegrees?) {
        self.streetAddress1 = streetAddress1
        self.streetAddress2 = streetAddress2
        self.streetAddress3 = streetAddress3
        self.city = city
        self.state = state
        self.zip = zip
        self.latitude = latitude
        self.longitude = longitude
    }
    
    enum AddressType {
        case Display
        case Full
    }
    
    required init(coder decoder: NSCoder) {
        self.streetAddress1 = decoder.decodeObject(forKey: NSCoderKeys.streetAddress1.rawValue) as! String
        self.streetAddress2 = decoder.decodeObject(forKey: NSCoderKeys.streetAddress2.rawValue) as? String
        self.streetAddress3 = decoder.decodeObject(forKey: NSCoderKeys.streetAddress3.rawValue) as? String
        self.city = decoder.decodeObject(forKey: NSCoderKeys.city.rawValue) as! String
        self.state = decoder.decodeObject(forKey: NSCoderKeys.state.rawValue) as! String
        self.zip = decoder.decodeObject(forKey: NSCoderKeys.zip.rawValue) as! String
        self.latitude = decoder.decodeObject(forKey: NSCoderKeys.latitude.rawValue) as? CLLocationDegrees
        self.longitude = decoder.decodeObject(forKey: NSCoderKeys.longitude.rawValue) as? CLLocationDegrees
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(streetAddress1, forKey: NSCoderKeys.streetAddress1.rawValue)
        coder.encode(streetAddress2, forKey: NSCoderKeys.streetAddress2.rawValue)
        coder.encode(streetAddress3, forKey: NSCoderKeys.streetAddress3.rawValue)
        coder.encode(city, forKey: NSCoderKeys.city.rawValue)
        coder.encode(state, forKey: NSCoderKeys.state.rawValue)
        coder.encode(zip, forKey: NSCoderKeys.zip.rawValue)
        coder.encode(latitude, forKey: NSCoderKeys.latitude.rawValue)
        coder.encode(longitude, forKey: NSCoderKeys.longitude.rawValue)
    }
    
    enum NSCoderKeys: String {
        case streetAddress1 = "location_streetAddress1"
        case streetAddress2 = "location_streetAddress2"
        case streetAddress3 = "location_streetAddress3"
        case city = "location_city"
        case state = "location_state"
        case zip = "location_zip"
        case latitude = "location_latitude"
        case longitude = "location_longitude"
    }
    
    enum GetterSetterNSCoderKeys: String {
        case key = "LOCATION_NSCODER_KEY"
    }

    
    func makeAddress(ofType type: AddressType) -> String {
        let streetAddress2 = self.streetAddress2 != nil ? ", \(self.streetAddress2!)": ""
        let streetAddress3 = self.streetAddress3 != nil ? ", \(self.streetAddress3!)": ""
        switch type {
        case .Display:
            return "\(streetAddress1)\(streetAddress2)\(streetAddress3)"
        case .Full:
            return "\(streetAddress1)\(streetAddress2)\(streetAddress3), \(city) \(state) \(zip)"
        }
    }
    
    class func makeAddress(ofType type: AddressType, streetAddress1: String, streetAddress2: String?, streetAddress3: String?, city: String, state: String, zip: String) -> String {
        let streetAddress2 = streetAddress2 != nil ? ", \(streetAddress2!)": ""
        let streetAddress3 = streetAddress3 != nil ? ", \(streetAddress3!)": ""
        switch type {
        case .Display:
            return "\(streetAddress1)\(streetAddress2)\(streetAddress3)"
        case .Full:
            return "\(streetAddress1)\(streetAddress2)\(streetAddress3), \(city) \(state) \(zip)"
        }
    }
}
