//
//  Groups.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation
import FirebaseDatabase

class Group: NSObject, NSCoding {
    var key: String
    var name: String
    var desc: String
    var contact: String
    var position: String
    var phone: String?
    var email: String?
    var website: String?
    var facebook: String?
    var twitter: String?
    var owner: String
    var locationName: String?
    var location: Location?
    var valid: Bool
    var users: [Key: Name]?
    var events: [Key: Bool]?
    
    init(key: String, name: String, description: String, contact: String, position: String, phone: String?, email: String?, website: String?, facebook: String?, twitter: String?, owner: String, locationName: String?, location: Location?, valid: Bool, users: [Key: Name]?, events: [Key: Bool]?) {
        self.key = key
        self.name = name
        self.desc = description
        self.contact = contact
        self.position = position
        self.phone = phone
        self.email = email
        self.website = website
        self.facebook = facebook
        self.twitter = twitter
        self.owner = owner
        self.locationName = locationName
        self.location = location
        self.valid = valid
        self.users = users
        self.events = events
    }
    
    required init(coder decoder: NSCoder) {
        self.key = decoder.decodeObject(forKey: NSCoderKeys.key.rawValue) as! String
        self.name = decoder.decodeObject(forKey: NSCoderKeys.name.rawValue) as! String
        self.desc = decoder.decodeObject(forKey: NSCoderKeys.description.rawValue) as! String
        self.contact = decoder.decodeObject(forKey: NSCoderKeys.contact.rawValue) as! String
        self.position = decoder.decodeObject(forKey: NSCoderKeys.position.rawValue) as! String
        self.phone = decoder.decodeObject(forKey: NSCoderKeys.phone.rawValue) as? String
        self.email = decoder.decodeObject(forKey: NSCoderKeys.email.rawValue) as? String
        self.website = decoder.decodeObject(forKey: NSCoderKeys.website.rawValue) as? String
        self.facebook = decoder.decodeObject(forKey: NSCoderKeys.facebook.rawValue) as? String
        self.twitter = decoder.decodeObject(forKey: NSCoderKeys.twitter.rawValue) as? String
        self.owner = decoder.decodeObject(forKey: NSCoderKeys.owner.rawValue) as! String
        self.locationName = decoder.decodeObject(forKey: NSCoderKeys.locationName.rawValue) as? String
        self.location = decoder.decodeObject(forKey: NSCoderKeys.location.rawValue) as? Location
        self.valid = decoder.decodeBool(forKey: NSCoderKeys.valid.rawValue)
        self.users = decoder.decodeObject(forKey: NSCoderKeys.users.rawValue) as? [Key: Name]
        self.events = decoder.decodeObject(forKey: NSCoderKeys.events.rawValue) as? [Key: Bool]
        
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(key, forKey: NSCoderKeys.key.rawValue)
        coder.encode(name, forKey: NSCoderKeys.name.rawValue)
        coder.encode(desc, forKey: NSCoderKeys.description.rawValue)
        coder.encode(contact, forKey: NSCoderKeys.contact.rawValue)
        coder.encode(position, forKey: NSCoderKeys.position.rawValue)
        coder.encode(phone, forKey: NSCoderKeys.phone.rawValue)
        coder.encode(email, forKey: NSCoderKeys.email.rawValue)
        coder.encode(website, forKey: NSCoderKeys.website.rawValue)
        coder.encode(facebook, forKey: NSCoderKeys.facebook.rawValue)
        coder.encode(twitter, forKey: NSCoderKeys.twitter.rawValue)
        coder.encode(owner, forKey: NSCoderKeys.owner.rawValue)
        coder.encode(locationName, forKey: NSCoderKeys.locationName.rawValue)
        coder.encode(location, forKey: NSCoderKeys.location.rawValue)
        coder.encode(valid, forKey: NSCoderKeys.valid.rawValue)
        coder.encode(users, forKey: NSCoderKeys.users.rawValue)
        coder.encode(events, forKey: NSCoderKeys.events.rawValue)
    }
    
    enum NSCoderKeys: String {
        case key = "group_key"
        case name = "group_name"
        case description = "group_description"
        case contact = "group_contact"
        case position = "group_position"
        case phone = "group_phone"
        case email = "group_email"
        case website = "group_website"
        case facebook = "group_facebook"
        case twitter = "group_twitter"
        case owner = "group_owner"
        case locationName = "group_locationName"
        case location = "group_location"
        case valid = "group_valid"
        case users = "group_users"
        case events = "group_events"
    }
    
    enum GetterSetterNSCoderKeys: String {
        case key = "GROUPS_NSCODER_KEY"
    }
    
    class func set(groups: [Group]) {
        let encoded = NSKeyedArchiver.archivedData(withRootObject: groups)
        defaults.set(encoded, forKey: GetterSetterNSCoderKeys.key.rawValue)
    }
    
    class func get() -> [Group]? {
        if let data = defaults.data(forKey: GetterSetterNSCoderKeys.key.rawValue) {
            let groups = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Group]
            return groups
        } else {
            return nil
        }
    }
    
    class func observe() {
        let ref = Database.database().reference().child("groups")
        ref.observe(.value, with: { snapshot in
            if let snapshot = snapshot.value as? [Key: AnyObject] {
                var groups: [Group] = []
                for group in snapshot {
                    if let value = group.value as? [Key: AnyObject] {
                        let key = group.key
                        let name = value["name"] as? String
                        let description = value["description"] as? String
                        let contact = value["contact"] as? String
                        let position = value["position"] as? String
                        let phone = value["phone"] as? String
                        let email = value["email"] as? String
                        let website = value["website"] as? String
                        let facebook = value["facebook"] as? String
                        let twitter = value["twitter"] as? String
                        let owner = value["userId"] as? String
                        let locationName = value["locationName"] as? String
                        let valid = value["valid"] as? Bool
                        let users = value["users"] as? [Key: Name]
                        let events = value["events"] as? [Key: Bool]
                        
                        var groupLocation: Location?
                        if let location = value["location"] as? [Key: AnyObject], let streetAddress1 = location["streetAddress1"] as? String, let city = location["city"] as? String, let state = location["state"] as? String, let zip = location["zip"] as? String {
                            let streetAddress2 = location["streetAddress2"] as? String
                            let streetAddress3 = location["streetAddress3"] as? String
                            let latitude = location["latitude"] as? Double
                            let longitude = location["longitude"] as? Double
                            let loc = Location(streetAddress1: streetAddress1, streetAddress2: streetAddress2, streetAddress3: streetAddress3, city: city, state: state, zip: zip, latitude: latitude, longitude: longitude)
                            groupLocation = loc
                        }
                        
                        if let name = name, let description = description, let contact = contact, let position = position, let owner = owner, let valid = valid {
                            groups.append(Group(key: key, name: name, description: description, contact: contact, position: position, phone: phone, email: email, website: website, facebook: facebook, twitter: twitter, owner: owner, locationName: locationName, location: groupLocation, valid: valid, users: users, events: events))
                        }
                    }
                }
                set(groups: groups)
                Event.updateGroupNames(using: groups)
                NotificationCenter.default.post(name: NSNotification.Name("incomeNewData"), object: nil)
            }
        })
    }
}

