//
//  NavigationController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 8/14/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 23, green: 70, blue: 186)
        self.navigationController?.navigationBar.tintColor = UIColor(red: 23, green: 70, blue: 186)
        self.navigationController?.navigationBar.isTranslucent = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
