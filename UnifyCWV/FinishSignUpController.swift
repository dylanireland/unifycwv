//
//  FinishSignUpController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 5/28/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import SCLAlertView

class FinishSignUpController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    
    @IBOutlet weak var finishAccountSetup: UIButton!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBOutlet weak var navItem: UINavigationItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navBar.titleTextAttributes = [NSFontAttributeName: UIFont().Default(withSize: 18), NSForegroundColorAttributeName: UIColor.white]
        
        navItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont().Default(withSize: 15)], for: .normal)
        
        fullName.delegate = self
        phoneNumber.delegate = self
        
        if phoneNumber.isNilOrEmpty() || fullName.isNilOrEmpty() {
            greyOut(enable: true)
        } else {
            greyOut(enable: false)
        }
        
        phoneNumber.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        fullName.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fullName.becomeFirstResponder()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumber {
            return !string.strictlyNumbersAllowed
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !phoneNumber.isNilOrEmpty(), !fullName.isNilOrEmpty() {
            self.view.endEditing(true)
            finishSignUp()
            return false
        }
        if fullName.isFirstResponder {
            phoneNumber.becomeFirstResponder()
        } else if phoneNumber.isFirstResponder {
            self.view.endEditing(true)
        }
        return false
    }
    
    func editingChanged() {
        if !phoneNumber.isNilOrEmpty(), !fullName.isNilOrEmpty() {
            greyOut(enable: false)
        } else {
            greyOut(enable: true)
        }
    }
    
    @IBAction func FinishAccountSetup() {
        finishSignUp()
    }
    
    func finishSignUp() {
        guard let user = Auth.auth().currentUser, User.getCurrentUser() != nil else {
            SCLAlertView().showError("Error", subTitle: "An error occured, please try again")
            return
        }
        Filter(disable: false)
        let values: [Key: Any] = ["name": fullName.text!, "phone": phoneNumber.text!, "complete": true]
        Database.database().reference().child("users").child(user.uid).updateChildValues(values, withCompletionBlock: { error, _ in
            guard error == nil else {
                self.Filter(disable: true)
                SCLAlertView().showError("Error", subTitle: "An error occured, please try again")
                return
            }
            User.getCurrentUser()?.update(name: values["name"] as? String, phone: values["phone"] as? String, complete: true)
            self.Filter(disable: true)
            self.view.endEditing(true)
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    func greyOut(enable: Bool) {
        if enable {
            finishAccountSetup.backgroundColor = UIColor(red: 76, green: 76, blue: 76)
            finishAccountSetup.setTitleColor(UIColor.lightGray, for: .normal)
            finishAccountSetup.isEnabled = false
        } else {
            finishAccountSetup.backgroundColor = UIColor(red: 23, green: 70, blue: 186)
            finishAccountSetup.setTitleColor(UIColor.white, for: .normal)
            finishAccountSetup.isEnabled = true
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func keyboardWasShown(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.bottomConstraint.constant = keyboardFrame.size.height
        })
    }
    
    func keyboardWasHidden(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.bottomConstraint.constant = 0
        })
    }

}
