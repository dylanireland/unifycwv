//
//  FullGroupViewController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 9/3/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import SCLAlertView

class FullGroupViewController: UIViewController {
    
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var websiteButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    
    var newEventButton = UIBarButtonItem()
    var editResourceButton = UIBarButtonItem()
    var manageButton = UIBarButtonItem()
    var deleteGroupButton = UIBarButtonItem()
    
    private var embeddedViewController: InGroupEventListController!
    
    var group: Group!
    
    var facebook: String = String()
    var website: String = String()
    var email: String = String()
    var twitter: String = String()
    
    var groupID: String?
    
    internal var admin = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        manageButton = UIBarButtonItem(image: #imageLiteral(resourceName: "manage"), style: .plain, target: self, action: #selector(barButtonPressed(sender:)))
        manageButton.accessibilityLabel = "manage"
        
        newEventButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(barButtonPressed(sender:)))
        newEventButton.accessibilityLabel = "newevent"
        
        editResourceButton = UIBarButtonItem(image: #imageLiteral(resourceName: "edit"), style: .plain, target: self, action: #selector(barButtonPressed(sender:)))
        editResourceButton.accessibilityLabel = "edit"
        
        deleteGroupButton = UIBarButtonItem(image: #imageLiteral(resourceName: "trash"), style: .plain, target: self, action: #selector(barButtonPressed(sender:)))
        deleteGroupButton.accessibilityLabel = "delete"
        
        navigationItem.setRightBarButtonItems([newEventButton, manageButton, editResourceButton, deleteGroupButton], animated: true)
        
        if let facebook = group.facebook {
            facebookButton.isEnabled = true
            self.facebook = facebook
        }
        
        if let email = group.email {
            emailButton.isEnabled = true
            self.email = email
        }
        
        if let twitter = group.twitter {
            twitterButton.isEnabled = true
            self.twitter = twitter
        }
        
        if let website = group.website {
            websiteButton.isEnabled = true
            self.website = website
        }
        
        descriptionLabel.text = group.desc
        if group.events == nil {
            postNoEventsView()
        }
        
        locationLabel.text = group.locationName ?? group.location?.makeAddress(ofType: .Display) ?? "Location Unknown"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        barButtonAvailability()
    }
    
    @IBAction func externalButtonPressed(sender: UIButton) {
        var url: String?
        switch sender.accessibilityIdentifier! {
        case "facebook":
            url = facebook
        case "website":
            url = website
        case "email":
            url = email
        case "twitter":
            url = twitter
        default: break
        }
        
        if url != nil {
            UIApplication.shared.openURL(URL(string: url!)!)
        }
    }
    
    func barButtonAvailability() {
        
        newEventButton.tintColor = UIColor.clear
        newEventButton.isEnabled = false
        
        editResourceButton.tintColor = UIColor.clear
        editResourceButton.isEnabled = false
        
        manageButton.tintColor = UIColor.clear
        manageButton.isEnabled = false
        
        deleteGroupButton.tintColor = UIColor.clear
        deleteGroupButton.isEnabled = false
        
        var users: [Key] = [Key]()
        users.append(group.owner)
        if let addableUsers = group.users {
            for user in addableUsers {
                users.append(user.key)
            }
        }
        users = users.removeDuplicates()
        
        if Auth.auth().currentUser != nil {
            if users.contains(Auth.auth().currentUser!.uid) || admin {
                newEventButton.tintColor = UIColor(red: 23, green: 70, blue: 186)
                newEventButton.isEnabled = true
                
                editResourceButton.tintColor = UIColor(red: 23, green: 70, blue: 186)
                editResourceButton.isEnabled = true
                
                manageButton.tintColor = UIColor(red: 23, green: 70, blue: 186)
                manageButton.isEnabled = true
                
                deleteGroupButton.tintColor = UIColor(red: 23, green: 70, blue: 186)
                deleteGroupButton.isEnabled = true
                removeButtons()
            } else {
                newEventButton.tintColor = UIColor(red: 23, green: 70, blue: 186)
                newEventButton.isEnabled = true
                newEventButton.title = "Request"
                newEventButton.accessibilityLabel = "request"
            }
        }
    }
    
    func removeButtons() {
        navigationItem.setRightBarButtonItems(nil, animated: false)
    }
    
    func barButtonPressed(sender: UIBarButtonItem) {
        if sender.accessibilityLabel == "edit" {
            if let EditRecourceVC = storyboard?.instantiateViewController(withIdentifier: "EditResourceController") as? EditResourceController {
                EditRecourceVC.group = group
                navigationController?.pushViewController(EditRecourceVC, animated: true)
            }
        } else if sender.accessibilityLabel == "request" {
            let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Yes") {
                self.requestForGroup()
            }
            alertView.addButton("No", action: {})
            alertView.showInfo("Request", subTitle: "Would you like to request to join this resource?")
        } else if sender.accessibilityLabel == "manage" {
            if let manageVC = storyboard?.instantiateViewController(withIdentifier: "ManageUsersController") as? ManageUsersController {
                manageVC.group = group
                navigationController?.pushViewController(manageVC, animated: true)
            }
        } else if sender.accessibilityLabel == "newevent" {
            if let NewEventVC = storyboard?.instantiateViewController(withIdentifier: "NewEventController") as? NewEventController {
                NewEventVC.group = group
                navigationController?.pushViewController(NewEventVC, animated: true)
            }
        } else if sender.accessibilityLabel == "delete" {
            let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Yes") {
                self.deleteGroup()
            }
            alertView.addButton("No", action: {})
            alertView.showInfo("Delete", subTitle: "Are you sure you want to delete this resource?")
        }
    }
    
    func requestForGroup() {
        guard let user = Auth.auth().currentUser, let name = User.getCurrentUser()?.name, let group = group else {
            SCLAlertView().showError("Error", subTitle: "An error occured, please try again")
            return
        }
        self.Filter(disable: false)
        
        let usertoname = [user.uid: name]
        let ref = Database.database().reference()
            
        let values: [AnyHashable: Any] = ["group-requests/\(group.key)": usertoname, "users/\(user.uid)/requests": [group.key: group.name]]
        
        ref.updateChildValues(values, withCompletionBlock: { error, _ in
            self.Filter(disable: true)
            guard error == nil else {
                SCLAlertView().showError("Error", subTitle: "An error occured, please try again")
                return
            }
            SCLAlertView().showSuccess("Success", subTitle: "You have requested to join this group")
        })
    }
    
    func deleteGroup() {
        let values: [AnyHashable: Any] = ["groups/\(group.key)": NSNull(), "group-events/\(group.key)": NSNull(), "group-requests/\(group.key)": NSNull()]
        
        Database.database().reference().updateChildValues(values, withCompletionBlock: { error, _ in
            self.Filter(disable: true)
            guard error == nil else {
                SCLAlertView().showError("Error", subTitle: "An error occured, please try again")
                return
            }
            SCLAlertView().showSuccess("Success", subTitle: "You have requested to join this group")
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? InGroupEventListController,
            segue.identifier == "InGroupEmbedSegue" {
            if let events = group.events {
                self.embeddedViewController = vc
                vc.passedEvents = events
                vc.groupName = group.name
            }
        }
    }
    
    func postNoEventsView() {
        var frame = CGRect()
        frame = containerView.frame
        containerView.isHidden = true
        var noEventsView: UIView {
            let v = UIView(frame: frame)
            v.backgroundColor = UIColor(red: 235, green: 235, blue: 235)
            return v
        }
        
        let label = UILabel()
        label.frame = CGRect(x: frame.midX - 50, y: frame.midY - 50, width: 100, height: 50)
        label.font = UIFont().Default(withSize: 15)
        label.textColor = UIColor.darkGray
        label.text = "No Events"
        label.textAlignment = .center
        
        self.view.addSubview(noEventsView)
        self.view.addSubview(label)
    }
}
