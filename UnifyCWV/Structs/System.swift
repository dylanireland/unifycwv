//
//  System.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/18/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct System {
    static private let currentVersion = 2
    
    static private var adminEmail = (defaults.value(forKey: UserDefaultKeys.adminEmail.rawValue) as? String) ?? "unifycharlestonwv@gmail.com"
    static private var version = defaults.integer(forKey: UserDefaultKeys.version.rawValue) != 0 ? defaults.integer(forKey: UserDefaultKeys.version.rawValue): 1
    static private var area = (defaults.value(forKey: UserDefaultKeys.area.rawValue) as? String) ?? "Charleston West Virginia"
    static let ref = Database.database().reference().child("system")
    
    enum UserDefaultKeys: String {
        case adminEmail = "system_adminEmail"
        case version = "system_database_version"
        case area = "system_area"
    }
    
    static func update(adminEmail: String? = nil, area: String? = nil, version: Int? = nil) {
        self.adminEmail = adminEmail ?? self.adminEmail
        self.version = version ?? self.version
        self.area = area ?? self.area
    }
    
    static func observe() {
        ref.observe(.value, with: { snapshot in
            if let snapshot = snapshot.value as? [Key: AnyObject] {
                for kVPair in snapshot {
                    let key = kVPair.key
                    switch key {
                        case "adminEmail": update(adminEmail: kVPair.value as? String)
                        case "area": update(area: kVPair.value as? String)
                        case "version": update(version: kVPair.value as? Int)
                        default: break
                    }
                }
            }
        })
    }
    
    static func stopObserving() {
        ref.removeAllObservers()
    }
    
    static func getVersion() -> Int {
        return self.version
    }
    
    static func getArea() -> String {
        return self.area
    }
    
    static func getAdminEmail() -> String {
        return self.adminEmail
    }
    
    static func checkDatabaseVersion() throws {
        if self.version == -1 {
            throw DatabaseVersionErrors.maintenance
        } else if self.version > self.currentVersion {
            throw DatabaseVersionErrors.needsUpdated
        }
    }
    
    enum DatabaseVersionErrors: Error {
        case maintenance
        case needsUpdated
    }
    
}
