//
//  Terms.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/19/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation

struct Terms {
    func validate() {
        defaults.set(true, forKey: "cuser_terms")
    }
    
    func get() -> Bool {
        return defaults.bool(forKey: "cuser_terms")
    }
    
    enum TermsSubs: String {
        case alertSubtitle = "Please review the terms and conditions before continuing"
        case exitSubtitle = "You cannot use UnifyCWV without accepting our terms and conditions, the app will now close"
    }
}
