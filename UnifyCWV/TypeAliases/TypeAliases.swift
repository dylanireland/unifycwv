//
//  TypeAliases.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import Foundation

typealias Key = String
typealias Name = String
