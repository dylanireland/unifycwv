/*//
//  MenuPageController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 2/18/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit

class MenuPageController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource, UIScrollViewDelegate {
    
    lazy var views: [UIViewController] = {
        return [self.instance(name: "Menu"), self.instance(name: "About")]
    }()
    
    private func instance(name: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: name)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        if let menu = views.first {
            setViewControllers([menu], direction: .forward, animated: true, completion: nil)
        }
    }
    
    override func viewDidLayoutSubviews() {
        for subView in self.view.subviews {
            if subView is UIScrollView {
                subView.frame = self.view.bounds
            } else if subView is UIPageControl {
                subView.backgroundColor = UIColor.clear
            }
        }
        super.viewDidLayoutSubviews()
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = views.index(of: viewController) else { return nil }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0          else { return nil }
        guard views.count > previousIndex else { return nil }
        return views[previousIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = views.index(of: viewController) else { return nil }
        let nextIndex = viewControllerIndex + 1
        guard nextIndex < views.count else { return nil }
        guard views.count > nextIndex else { return nil }
        return views[nextIndex]
    }
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return views.count
    }
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first, let firstViewControllerIndex = views.index(of: firstViewController) else {
            return 0
        }
        
        return firstViewControllerIndex
    }
}*/
