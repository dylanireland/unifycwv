//
//  CalendarCell.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 11/22/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarCell: JTAppleCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var bubbleView: UIView!
}
