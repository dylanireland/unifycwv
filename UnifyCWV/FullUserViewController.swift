//
//  FullUserViewController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/7/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import FirebaseDatabase
import SCLAlertView

class FullUserViewController: UIViewController {
    
    let ref = Database.database().reference()
    
    var user: User!
    
    @IBOutlet weak var validateeLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var validateButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        validateeLabel.text = user.name ?? "Name Unknown"
        emailLabel.text = user.email ?? "Email Unknown"
        phoneLabel.text = user.phone ?? "Phone number unknown"
    }
    
    @IBAction func validate() {
        let valid_ref = ref.child("users").child(user.uid).child("valid")
        Filter(disable: false)
        valid_ref.setValue(true) { error, _ in
            guard error == nil else {
                self.Filter(disable: true)
                SCLAlertView().showError("Error", subTitle: error!.localizedDescription)
                return
            }
            self.Filter(disable: true)
            self.dismiss(animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
