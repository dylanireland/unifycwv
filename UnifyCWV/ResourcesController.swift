//
//  ResourcesController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 8/21/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import SwiftyJSON

class ResourcesController: UITableViewController {
    
    var ref: DatabaseReference!
    
    var groups: [Group]?
    
    private var admin = false
    
    @IBOutlet weak var sortSegmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(incomeNewEvents), name: NSNotification.Name("incomeNewData"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let storyboardID = self.restorationIdentifier {
            if storyboardID == "ResourcesControllerAdmin" {
                admin = true
            }
        }
        
        NotificationCenter.default.post(name: NSNotification.Name("tabBarViewChanged"), object: nil, userInfo: ["key": 3])
        incomeNewEvents()
    }
    
    func incomeNewEvents() {
        groups = Group.get()
        guard groups != nil, groups!.count != 0 else {
            return
        }
        if admin {
            var newList = [Group]()
            for index in 0..<groups!.count {
                let group = groups![index]
                if !group.valid {
                    let element = groups!.remove(at: index)
                    groups!.insert(element, at: index)
                    newList.append(element)
                }
            }
            groups = newList
        }
        
        sort(by: .Name)
        self.tableView.reloadData()
        Filter(disable: true)
    }
    
    @IBAction func sorterChanged(_ sender: UISegmentedControl) {
        switch sortSegmentedControl.selectedSegmentIndex {
        case SorterIndices.Name.hashValue:
            sort(by: SorterIndices.Name)
        case SorterIndices.EventCount.hashValue:
            sort(by: SorterIndices.EventCount)
        default: break
        }
    }
    
    enum SorterIndices {
        case Name, EventCount
    }
    
    func sort(by index: SorterIndices) {
        guard groups != nil else {
            return
        }
        if index == .Name {
            groups!.sort { $0.name < $1.name }
        }
        if index == .EventCount {
            groups!.sort { $0.events?.count ?? 0 > $1.events?.count ?? 0 }
        }
        self.tableView.reloadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let fullGroupController = storyboard?.instantiateViewController(withIdentifier: "FullGroupViewController") as? FullGroupViewController, let groups = groups else {
            return
        }
        fullGroupController.group = groups[indexPath.row]
        fullGroupController.admin = admin
        navigationController?.pushViewController(fullGroupController, animated: true)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let groups = groups else {
            return 0
        }
        return groups.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        guard let groups = groups, groups.count != 0 else {
            return cell
        }
        let group = groups[indexPath.row]
        let groupNameLabel = cell.viewWithTag(10) as! UILabel
        groupNameLabel.text = group.name
        let descriptionLabel = cell.viewWithTag(11) as! UILabel
        descriptionLabel.text = group.desc
        let eventCountLabel = cell.viewWithTag(12) as! UILabel
        if let events = group.events {
            eventCountLabel.text = "Events: \(String(events.count))"
        } else {
            eventCountLabel.text = "No Events"
        }
        return cell
    }
}
