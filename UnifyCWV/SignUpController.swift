//
//  SignUpController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 5/26/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import EGFormValidator
import FirebaseAuth
import SCLAlertView
import FirebaseDatabase

class SignUpController: ValidatorViewController, UITextFieldDelegate {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var passwordRepeatField: UITextField!
    
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var passwordRepeatErrorLabel: UILabel!
    
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var navItem: UINavigationItem!
    
    @IBOutlet weak var navBar: UINavigationBar!
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        navBar.titleTextAttributes = [NSFontAttributeName: UIFont().Default(withSize: 18), NSForegroundColorAttributeName: UIColor.white]
        
        navItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont().Default(withSize: 15)], for: .normal)
        
        self.addValidatorEmail(toControl: emailField, errorPlaceholder: emailErrorLabel, errorMessage: "Email is not valid")
        
        self.addValidatorMinLength(toControl: passwordField, errorPlaceholder: passwordErrorLabel, errorMessage: "Password must be at least 8 characters", minLength: 8)
        
        self.addValidatorMandatory(toControl: emailField, errorPlaceholder: emailErrorLabel, errorMessage: "This field is required")
        
        self.addValidatorMandatory(toControl: passwordField, errorPlaceholder: passwordErrorLabel, errorMessage: "This field is required")
        
        self.addValidatorMandatory(toControl: passwordRepeatField, errorPlaceholder: passwordRepeatErrorLabel, errorMessage: "This field is required")
        
        self.addValidatorEqualTo(toControl: passwordRepeatField, errorPlaceholder: passwordRepeatErrorLabel, errorMessage: "Passwords do not match", compareWithControl: passwordField)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        emailField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if emailField.isFirstResponder {
            emailField.resignFirstResponder()
        } else if passwordField.isFirstResponder {
            passwordField.resignFirstResponder()
        } else if passwordRepeatField.isFirstResponder {
            passwordRepeatField.resignFirstResponder()
        }
    }
    
    @IBAction func SignUp() {
        if self.validate() {
            if emailField.isFirstResponder {
                emailField.resignFirstResponder()
            } else if passwordField.isFirstResponder {
                passwordField.resignFirstResponder()
            } else if passwordRepeatField.isFirstResponder {
                passwordRepeatField.resignFirstResponder()
            }
            signUp(email: emailField.text!, password: passwordField.text!)
        }
    }
    
    func signUp(email: String, password: String) {
        Filter(disable: false)
        Auth.auth().createUser(withEmail: email, password: password, completion: { user, error in
            guard error == nil else {
                self.Filter(disable: true)
                self.view.isUserInteractionEnabled = true
                SCLAlertView().showError("Error", subTitle: error!.localizedDescription)
                user?.delete(completion: nil)
                return
            }
            if let user = user {
                let username = email.components(separatedBy: "@")[0]
                let values = ["email": email, "admin": false, "username": username, "valid": false, "complete": false] as [Key : AnyObject]
                self.ref.child("users").child(user.uid).updateChildValues(values, withCompletionBlock: { error, _ in
                    guard error == nil else {
                        self.settle()
                        self.throwError()
                        return
                    }
                    User(uid: user.uid, name: nil, email: email, admin: false, valid: false, phone: nil, complete: false, groups: nil, requests: nil).set()
                    User.startObservingUserInfo()
                    self.settle()
                    self.dismiss(animated: true, completion: nil)
                })
            } else {
                Auth.auth().currentUser?.delete(completion: nil)
                self.settle()
                self.throwError()
            }
        })
    }
    
    func settle() {
        self.Filter(disable: true)
        self.view.isUserInteractionEnabled = true
    }
    
    func throwError() {
        SCLAlertView().showError("Error", subTitle: "An error occured, please try again")
    }
    
    @IBAction func backPressed() {
        segue()
    }
    
    func segue() {
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(containerView)
        NSLayoutConstraint.activate([
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            containerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            ])
        
        let controller = storyboard!.instantiateViewController(withIdentifier: "SignInController")
        
        addChildViewController(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(controller.view)
        
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            controller.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            controller.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            controller.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
            ])
        
        controller.didMove(toParentViewController: self)
    }
    
    func keyboardWasShown(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.bottomConstraint.constant = keyboardFrame.size.height
        })
    }
    
    func keyboardWasHidden(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.bottomConstraint.constant = 0
        })
    }
}
