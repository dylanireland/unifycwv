//
//  NewEventController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 9/15/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import EGFormValidator
import SCLAlertView
import CoreLocation
import DatePickerDialog

class NewEventController: ValidatorViewController, UITextFieldDelegate {
    
    var group: Group!

    @IBOutlet var inputFields: [UITextField]!
    var eventNameField: UITextField? = nil //
    var descField: UITextField? = nil //
    var contactField: UITextField? = nil //op
    var phoneField: UITextField? = nil //op
    var emailField: UITextField? = nil //op
    var websiteField: UITextField? = nil //op
    var facebookField: UITextField? = nil //op
    var stAddress1Field: UITextField? = nil //op
    var stAddress2Field: UITextField? = nil //op
    var stAddress3Field: UITextField? = nil //op
    var cityField: UITextField? = nil //op
    var stateField: UITextField? = nil //op
    var zipField: UITextField? = nil //op
    
    var startTime: String? = nil
    var endTime: String? = nil
    var startDate: String? = nil
    var endDate: String? = nil
    
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    
    @IBOutlet weak var descriptionOfAddResource: UITextView!
    
    @IBOutlet weak var publish: UIButton!
    
    let info: UIButton = UIButton(type: .infoLight)
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        greyOut(enable: true)
        
        for inputField in inputFields {
            inputField.returnKeyType = .next
            inputField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        }
        
        eventNameField = inputFields[0]
        contactField = inputFields[1]
        phoneField = inputFields[2]
        websiteField = inputFields[3]
        facebookField = inputFields[4]
        descField = inputFields[5]
        emailField = inputFields[6]
        stAddress1Field = inputFields[7]
        stAddress2Field = inputFields[8]
        stAddress3Field = inputFields[9]
        cityField = inputFields[10]
        stateField = inputFields[11]
        zipField = inputFields[12]
        
        zipField?.returnKeyType = .go
        
        self.eventNameField?.delegate = self
        self.contactField?.delegate = self
        self.phoneField?.delegate = self
        self.websiteField?.delegate = self
        self.facebookField?.delegate = self
        self.descField?.delegate = self
        self.emailField?.delegate = self
        self.stAddress1Field?.delegate = self
        self.stAddress2Field?.delegate = self
        self.stAddress3Field?.delegate = self
        self.cityField?.delegate = self
        self.stateField?.delegate = self
        self.zipField?.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.addValidatorEmail(toControl: self.emailField, errorPlaceholder: nil, errorMessage: "error")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.isUserInteractionEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        inputFields[0].becomeFirstResponder()
        self.view.isUserInteractionEnabled = true
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        scrollView.contentSize.height = 950
    }
    
    @IBAction func Publish() {
        validateFields()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case eventNameField!:
            self.contactField?.becomeFirstResponder()
        case contactField!:
            self.phoneField?.becomeFirstResponder()
        case phoneField!:
            self.websiteField?.becomeFirstResponder()
        case websiteField!:
            self.facebookField?.becomeFirstResponder()
        case facebookField!:
            self.descField?.becomeFirstResponder()
        case descField!:
            self.emailField?.becomeFirstResponder()
        case emailField!:
            self.stAddress1Field?.becomeFirstResponder()
        case stAddress1Field!:
            self.stAddress2Field?.becomeFirstResponder()
        case stAddress2Field!:
            self.stAddress3Field?.becomeFirstResponder()
        case stAddress3Field!:
            self.cityField?.becomeFirstResponder()
        case cityField!:
            self.stateField?.becomeFirstResponder()
        case stateField!:
            self.zipField?.becomeFirstResponder()
        case zipField!:
            validateFields()
        default: break
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneField {
            return !string.strictlyNumbersAllowed
        } else if textField == contactField {
            let aSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        info.setTitleColor(UIColor(red: 23, green: 70, blue: 186), for: .normal)
        info.frame.size = CGSize(width: 30, height: 30)
        info.frame = CGRect(x: textField.frame.maxX - info.frame.size.width, y: textField.frame.minY, width: 30, height: 30)
        scrollView.addSubview(info)
        
        let infoTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(infoPressed))
        infoTap.accessibilityLabel = textField.placeholder
        info.addGestureRecognizer(infoTap)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        info.removeFromSuperview()
    }
    
    func editingChanged() {
        if eventNameField!.isNilOrEmpty() || contactField!.isNilOrEmpty() || descField!.isNilOrEmpty() || startTime == nil || endTime == nil {
            greyOut(enable: true)
        } else {
            greyOut(enable: false)
        }
    }
    
    func validateFields() {
        if eventNameField!.isNilOrEmpty() || contactField!.isNilOrEmpty() || descField!.isNilOrEmpty() || startTime == nil || endTime == nil || startDate == nil || endDate == nil {
            greyOut(enable: true)
            return
        }
        
        greyOut(enable: false)
        
        if emailField!.isNilOrEmpty() {
            addEvent()
            return
        }
        
        if self.validate() {
            addEvent()
        } else {
            self.view.isUserInteractionEnabled = true
            SCLAlertView().showInfo("Whoops", subTitle: "This is not a valid email address")
        }
    }
        
    func addEvent() {
        self.view.isUserInteractionEnabled = false
        self.Filter(disable: false)
        
        var values: [String: AnyObject] = [:]
        var locationValues: [String: AnyObject] = [:]
        
        values["name"] = eventNameField!.text! as AnyObject
        values["contact"] = contactField!.text! as AnyObject
        values["description"] = descField!.text! as AnyObject
        values["valid"] = group.valid as AnyObject
        values["groupId"] = group.key as AnyObject
        values["startTime"] = startTime! as AnyObject
        values["endTime"] = endTime! as AnyObject
        values["date"] = startDate! as AnyObject
        values["endDate"] = endDate! as AnyObject
        
        if !phoneField!.isNilOrEmpty() {
            values["phone"] = phoneField?.text! as AnyObject
        }
        
        if !websiteField!.isNilOrEmpty() {
            values["website"] = websiteField?.text!  as AnyObject
        }
        
        if !facebookField!.isNilOrEmpty() {
            values["facebook"] = facebookField?.text!  as AnyObject
        }
        
        if !emailField!.isNilOrEmpty() {
            values["email"] = emailField?.text! as AnyObject
        }
        
        if !stAddress1Field!.isNilOrEmpty(), !cityField!.isNilOrEmpty(), !stateField!.isNilOrEmpty(), !zipField!.isNilOrEmpty() {
            locationValues["streetAddress1"] = stAddress1Field!.text! as AnyObject
            if !stAddress2Field!.isNilOrEmpty() {
                locationValues["streetAddress2"] = stAddress2Field!.text! as AnyObject
            }
            
            if !stAddress3Field!.isNilOrEmpty() {
                locationValues["streetAddress3"] = stAddress3Field!.text! as AnyObject
            }
            locationValues["city"] = cityField!.text! as AnyObject
            locationValues["state"] = stateField!.text! as AnyObject
            locationValues["zip"] = zipField!.text! as AnyObject
            
            let address = Location.makeAddress(ofType: .Full, streetAddress1: stAddress1Field!.text!, streetAddress2: stAddress2Field!.text, streetAddress3: stAddress3Field!.text, city: cityField!.text!, state: stateField!.text!, zip: zipField!.text!)
            let geoCoder = CLGeocoder()
            geoCoder.geocodeAddressString(address) { (placemarks, error) in
                if placemarks != nil {
                    if let location = placemarks!.first!.location {
                        let latitude = String(location.coordinate.latitude as Double)
                        let longitude = String(location.coordinate.longitude as Double)
                        locationValues["latitude"] = latitude as AnyObject
                        locationValues["longitude"] = longitude as AnyObject
                    }
                }
                values["location"] = locationValues as AnyObject
                self.finishAddEvent(values: values)
                return
            }
        } else {
            finishAddEvent(values: values)
            return
        }
    }
    
    func finishAddEvent(values: [String: AnyObject]) {
        let autoID = Database.database().reference().childByAutoId().key
        
        let values: [AnyHashable: Any] = ["events/\(autoID)": values, "group-events/\(group.key)/\(autoID)": values, "groups/\(group.key)/events/\(autoID)": true]
        
        Database.database().reference().updateChildValues(values, withCompletionBlock: { error, _ in
            guard error == nil else {
                self.Filter(disable: true)
                SCLAlertView().showError("Error", subTitle: "An error occured, please try again")
                return
            }
            self.Filter(disable: true)
            _ = self.navigationController?.popViewController(animated: true)
        })
    }
    
    @IBAction func timeClicked(sender: UIButton) {
        dismissKeyboard()
        var dateComponents = DateComponents()
        dateComponents.year = 2
        let maxDate = Calendar.current.date(byAdding: dateComponents, to: Date())
        DatePickerDialog().show("Select \(sender.currentTitle ?? "Date")", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: Date(), maximumDate: maxDate, datePickerMode: .dateAndTime, callback: { date in
            guard let date = date else {
                return
            }
            var labelToAlter = UILabel()
            if sender.currentTitle == "Start Time" {
                labelToAlter = self.startTimeLabel
            } else if sender.currentTitle == "End Time" {
                labelToAlter = self.endTimeLabel
            }
            if labelToAlter == self.startTimeLabel {
                self.startDate = date.makeEuropeanShorthandStringFromYMD()
                self.startTime = date.makeStringFromHHmm()
                
            } else if labelToAlter == self.endTimeLabel {
                self.endDate = date.makeEuropeanShorthandStringFromYMD()
                self.endTime = date.makeStringFromYMD()
            }
            
            labelToAlter.text = "\(date.makeStringFromHHmm().toTwelve()) \(date.makeShorthandStringFromYMD())"
            
            self.editingChanged()
        })
    }
    
    func infoPressed(sender: UITapGestureRecognizer) {
        var title = sender.accessibilityLabel!
        if title.range(of: " (Optional)") != nil {
            title = title.replacingOccurrences(of: " (Optional)", with: "")
        } else if title.range(of: " (Required for location)") != nil {
            title = title.replacingOccurrences(of: " (Required for location)", with: "")
        }
        switch title {
        case "Event Name":
            postHelp(title: title, subtitle: EventHelp.name.rawValue)
        case "Contact Person":
            postHelp(title: title, subtitle: EventHelp.contact_person.rawValue)
        case "Phone Number":
            postHelp(title: title, subtitle: EventHelp.phone_number.rawValue)
        case "Website":
            postHelp(title: title, subtitle: EventHelp.website.rawValue)
        case "Facebook ID":
            postHelp(title: title, subtitle: EventHelp.facebook_id.rawValue)
        case "Description":
            postHelp(title: title, subtitle: EventHelp.description.rawValue)
        case "Email":
            postHelp(title: title, subtitle: EventHelp.email.rawValue)
        case "Street Address 1":
            postHelp(title: title, subtitle: EventHelp.street_address_1.rawValue)
        case "Street Address 2":
            postHelp(title: title, subtitle: EventHelp.street_address_2.rawValue)
        case "Street Address 3":
            postHelp(title: title, subtitle: EventHelp.street_address_3.rawValue)
        case "City":
            postHelp(title: title, subtitle: EventHelp.city.rawValue)
        case "State":
            postHelp(title: title, subtitle: EventHelp.state.rawValue)
        case "Zip Code":
            postHelp(title: title, subtitle: EventHelp.zip.rawValue)
        default: break
        }
    }
    
    func postHelp(title: String, subtitle: String) {
        SCLAlertView().showInfo(title, subTitle: subtitle)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func greyOut(enable: Bool) {
        if enable {
            publish.setTitleColor(UIColor.gray, for: .normal)
            publish.isEnabled = false
        } else {
            publish.setTitleColor(UIColor.white, for: .normal)
            publish.isEnabled = true
        }
    }
        
    func keyboardWasShown(notification: NSNotification) {
        scrollView.contentSize.height = 1200
    }
    
    func keyboardWasHidden(notification: NSNotification) {
        scrollView.contentSize.height = 950
    }

}
