//
//  CalendarViewController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 11/22/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarViewController: UIViewController {
    @IBOutlet weak var monthLabelHeader: UILabel!
    @IBOutlet weak var yearLabelHeader: UILabel!
    
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    
    let formatter = DateFormatter()
    var keys = [String]()
    
    let month = Calendar.current.dateComponents([.month], from: Date()).month
    let year = Calendar.current.dateComponents([.year], from: Date()).year

    override func viewDidLoad() {
        super.viewDidLoad()
        monthLabelHeader.text = month != nil ? month!.monthToMonth() : ""
        if let year = year {
            yearLabelHeader.text = String(describing: year)
        }
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.post(name: NSNotification.Name("tabBarViewChanged"), object: nil, userInfo: ["key": 2])
    }
}

extension CalendarViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let calendar = Calendar.current
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = calendar.timeZone
        formatter.locale = calendar.locale
        
        let startDate = Date()
        
        var dateComponents = DateComponents()
        dateComponents.year = 2
        
        let endDate = Calendar.current.date(byAdding: dateComponents, to: startDate)
        
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate!, numberOfRows: 5, calendar: Calendar.current, generateInDates: .forAllMonths, generateOutDates: .tillEndOfGrid, firstDayOfWeek: .sunday, hasStrictBoundaries: true)
        
        return parameters
    }
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "cell", for: indexPath) as! CalendarCell
        cell.dateLabel.text = cellState.text
        
        if cellState.dateBelongsTo == .thisMonth {
            cell.dateLabel.textColor = UIColor.white
            formatter.dateFormat = "yyyy/MM/dd"
            if let keys = date.equate() {
                cell.bubbleView.isHidden = false
                cell.dateLabel.textColor = UIColor.Default
                cell.accessibilityElements = keys
            } else {
                cell.bubbleView.isHidden = true
                cell.dateLabel.textColor = UIColor.white
                cell.accessibilityElements = nil
            }
        } else {
            cell.bubbleView.isHidden = true
            cell.dateLabel.textColor = UIColor.gray
            cell.accessibilityElements = nil
        }
        
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        let date = visibleDates.monthDates.first!.date
        formatter.dateFormat = "MMMM"
        monthLabelHeader.text = formatter.string(from: date)
        formatter.dateFormat = "yyyy"
        yearLabelHeader.text = formatter.string(from: date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        guard let keys = cell?.accessibilityElements as? [String] else {
            return
        }
        self.keys = keys
        self.performSegue(withIdentifier: "fromCalendar", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? EventListController,
            segue.identifier == "fromCalendar" {
            vc.keys = keys
        }
    }
}
