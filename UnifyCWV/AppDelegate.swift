//
//  AppDelegate.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 2/17/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import Firebase
import SWRevealViewController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    
    //Privacy
    
    /* @TODO */
    
    /* FINAL VERSION NOTES: 1
        Make a readme
        add contact position field
     
        facebook id should JUST be ID, but we will still parse data
     
        Get thicker trash can image
     
     
     
        Scroll view on fulleventviewcontroller
        Scroll view on fullgroupviewcontroller
     
     
     
        Show who is in the resource on manage users view controller (use sections)
     
        -
     
        Make search available
     Find out why calendar is blanked out on FullEventVC
     Resources need to be alphabetical
     Make search available by name (Resources)
     Description field needs to be bigger
     Add groups field to user
     add locationName field (Events and Resources)
     Fix start time/ end time
     Event list does not work immediately after creation on fullresourcecontroller
     Once user is approved the resource should be approved
     Resource is not showing as not approved
     add resource validator admin page
     update drive
     change field to fullname not name
     add requests field to user
     user does not have to be valid to create resource
     Be able to devalidate users of resource
     Be able to pass on ownership to another use in Resources > FullResourceVC > ManageUsers
     show all users in admin user page
     be able to make another user an admin from admin > users
     an admin can declare a user to be an owner of a resource
     when user is approved send the an email (maybe in future, push notification)
     create second section in admin > users to manage other admins
     edit event is not working
     have resource automatically update after change
     if no http add http programmatically
     add link to privacy statement from About page underneath developed by (https://termsfeed.com/privacy-policy/5b64b113b5e85af0159f5d0e0f0a5e7e)
     add version number
     */
    
    /* DONE V1
     change name to fullname
     Fix filter thing
     Make event list queried by date/ name
     listen for group-requests changes
        be able to see what groups ive requested to be a part of and ones i already am
        do the group requests stuff
        All the help shit (if user is valid add more help stuff)
        if admin see admin
        Add eula and privacy policy
        add help page on all user input fields
        Allow all users listed in [users] to edit event or edit resource
        get strings info from marshalls thing: https://bitbucket.org/mggates39/unifycwv/src/b707c75d7cef329a6dc908ec4b1cb3581f8e92a6/UnifyCWV/app/src/main/res/values/strings.xml?at=master&fileviewer=file-view-default
        When adding event list to groups reference on database change from [key: true] to [key: Event Name]
        remove admin thing feedback
        start and end time on fulleventviewcontroller
        only show events that need approval - admin
        if resource is valid events are automatically valid
        Edit Resource and Edit event controller
        finish datepicker (fields) on NewEventController
        Change userId key on groups database property to groups[] and allow multiple users in array
        Forced update shit
        change system database field
        Make a feedback/contact-us on AboutController.swift
        do database version
        database -1 "under maintenance"
        Fix UI fields (reaching website and social from FullGroupViewController)
        Rearrange add organization fields
        Elongate (scroll view) add organization
        Automatic database updates (refer to automatic validation query in AppDelegate)
        start-end will have dates so "date" key goes away
        Throw reasoning for database_needs_updated key UserDefaults
    */
    
    /* NEXT VERSION NOTES: 2
        Add network timeouts
        Have before-edit pages update after edit is published
     
        Update my info to show that people are part of and/or have requested to be part of groups
        Add "http://" or "http://facebook.com/" etc in fields after user moves to next field
        Add New Event on EventListController right corner an if user is part of multiple organizations he or she will get a separate view asking which one they want to add an event to
    */
    
    /* V3 Bitch
        You got a lot of facebook REST API shit to do buddy
        Check beta test feedback
    */

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UITabBar.appearance().tintColor = UIColor(red: 23, green: 70, blue: 186)
        
        let appearance = UITabBarItem.appearance()
        let attributes = [NSFontAttributeName: UIFont().Default(withSize: 10)]
        appearance.setTitleTextAttributes(attributes, for: .normal)
        
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
        
        System.observe()
        Group.observe()
        Event.observe()
        User.startObservingUsers()
        User.startObservingUsers()
        GroupRequest.observe()
        
        return true
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

