//
//  AdminUsersController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 11/5/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit

class AdminUsersController: UITableViewController {
    
    var valids: [User]?
    var invalids: [User]?
    var selectableUser: User!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let users = User.getUsers() else {
            return
        }
        let separatable = User.separateValids(from: users)
        valids = separatable.valids
        invalids = separatable.invalids
        self.tableView.reloadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return invalids?.count ?? 0
        } else if section == 1 {
            return valids?.count ?? 0
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Invalid"
        } else if section == 1 {
            return "Valid"
        }
        return "Users"
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        guard let valids = valids, let invalids = invalids else {
            return cell
        }
        let validatee = cell.viewWithTag(10) as! UILabel
        if indexPath.section == 0 {
            let user = valids[indexPath.row]
            validatee.text = user.name
        } else if indexPath.section == 1 {
            let user = invalids[indexPath.row]
            validatee.text = user.name
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let valids = valids, let invalids = invalids else {
            return
        }
        if indexPath.section == 0 {
            let user = valids[indexPath.row]
            self.selectableUser = user
        } else if indexPath.section == 1 {
            let user = invalids[indexPath.row]
            self.selectableUser = user
        }
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "fromAdminUserstoFullUser", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! FullUserViewController
        vc.user = selectableUser
    }
}
