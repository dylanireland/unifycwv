//
//  AddOrganizationController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 4/2/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import EGFormValidator
import FirebaseAuth
import FirebaseDatabase
import SCLAlertView
import CoreLocation

class EditResourceController: ValidatorViewController, UITextFieldDelegate {
    
    @IBOutlet var inputFields: [UITextField]!
    var orgName: UITextField? = nil //
    var contactPerson: UITextField? = nil //
    var phoneNumber: UITextField? = nil //op
    var website: UITextField? = nil //op
    var facebookURL: UITextField? = nil //op
    var twitterURL: UITextField? = nil
    var email: UITextField? = nil //op
    var desc: UITextField? = nil //
    var stAddress1: UITextField? = nil //op
    var stAddress2: UITextField? = nil //op
    var stAddress3: UITextField? = nil //op
    var city: UITextField? = nil //op
    var state: UITextField? = nil //op
    var zip: UITextField? = nil //op
    //contact position
    
    @IBOutlet weak var publish: UIButton!
    
    let info: UIButton = UIButton(type: .infoLight)
    
    var group: Group!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        greyOut(enable: true)
        
        for inputField in inputFields {
            inputField.returnKeyType = .next
            inputField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        }
        
        orgName = inputFields[0]
        contactPerson = inputFields[1]
        phoneNumber = inputFields[2]
        website = inputFields[3]
        facebookURL = inputFields[4]
        twitterURL = inputFields[5]
        email = inputFields[6]
        desc = inputFields[7]
        stAddress1 = inputFields[8]
        stAddress2 = inputFields[9]
        stAddress3 = inputFields[10]
        city = inputFields[11]
        state = inputFields[12]
        zip = inputFields[13]
        
        zip?.returnKeyType = .go
        
        self.orgName?.delegate = self
        self.contactPerson?.delegate = self
        self.phoneNumber?.delegate = self
        self.website?.delegate = self
        self.facebookURL?.delegate = self
        self.twitterURL?.delegate = self
        self.email?.delegate = self
        self.desc?.delegate = self
        self.stAddress1?.delegate = self
        self.stAddress2?.delegate = self
        self.stAddress3?.delegate = self
        self.city?.delegate = self
        self.state?.delegate = self
        self.zip?.delegate = self
        
        orgName?.text = group.name
        contactPerson?.text = group.contact
        phoneNumber?.text = group.phone ?? nil
        website?.text = group.website ?? nil
        facebookURL?.text = group.facebook ?? nil
        twitterURL?.text = group.twitter ?? nil
        email?.text = group.email ?? nil
        desc?.text = group.desc
        stAddress1?.text = group.location?.streetAddress1
        stAddress2?.text = group.location?.streetAddress2
        stAddress3?.text = group.location?.streetAddress3
        city?.text = group.location?.city
        state?.text = group.location?.state
        zip?.text = group.location?.zip
        
        phoneNumber?.keyboardType = .numberPad
        
        self.addValidatorEmail(toControl: self.email, errorPlaceholder: nil, errorMessage: "error")
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.isUserInteractionEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        inputFields[0].becomeFirstResponder()
        self.view.isUserInteractionEnabled = true
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        scrollView.contentSize.height = 840
    }
    
    @IBAction func Publish() {
        validateFields()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case orgName!:
            self.contactPerson?.becomeFirstResponder()
        case contactPerson!:
            self.phoneNumber?.becomeFirstResponder()
        case phoneNumber!:
            self.website?.becomeFirstResponder()
        case website!:
            self.facebookURL?.becomeFirstResponder()
        case facebookURL!:
            self.twitterURL?.becomeFirstResponder()
        case twitterURL!:
            self.email?.becomeFirstResponder()
        case email!:
            self.desc?.becomeFirstResponder()
        case desc!:
            self.stAddress1?.becomeFirstResponder()
        case stAddress1!:
            self.stAddress2?.becomeFirstResponder()
        case stAddress2!:
            self.stAddress3?.becomeFirstResponder()
        case stAddress3!:
            self.city?.becomeFirstResponder()
        case city!:
            self.state?.becomeFirstResponder()
        case state!:
            self.zip?.becomeFirstResponder()
        case zip!:
            validateFields()
        default: break
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumber {
            return !string.strictlyNumbersAllowed
        } else if textField == contactPerson {
            let aSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        info.setTitleColor(UIColor(red: 23, green: 70, blue: 186), for: .normal)
        info.frame.size = CGSize(width: 30, height: 30)
        info.frame = CGRect(x: textField.frame.maxX - info.frame.size.width, y: textField.frame.minY, width: 30, height: 30)
        scrollView.addSubview(info)
        
        let infoTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(infoPressed))
        infoTap.accessibilityLabel = textField.placeholder
        info.addGestureRecognizer(infoTap)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        info.removeFromSuperview()
    }
    
    func editingChanged() {
        if orgName!.isNilOrEmpty() || contactPerson!.isNilOrEmpty() || desc!.isNilOrEmpty() {
            greyOut(enable: true)
        } else {
            greyOut(enable: false)
        }
    }
    
    func validateFields() {
        if orgName!.isNilOrEmpty() || contactPerson!.isNilOrEmpty() || desc!.isNilOrEmpty() {
            greyOut(enable: true)
            return
        }
        greyOut(enable: false)
        if email!.isNilOrEmpty() {
            refreshResource()
            return
        }
        if self.validate() {
            refreshResource()
        } else {
            self.view.isUserInteractionEnabled = true
            SCLAlertView().showInfo("Whoops", subTitle: "This is not a valid email address")
        }
    }
    
    func refreshResource() {
        self.view.isUserInteractionEnabled = false
        
        self.Filter(disable: false)
        
        var values: [String: AnyObject] = [:]
        var locationValues: [String: AnyObject] = [:]
        
        values["name"] = orgName?.text! as AnyObject
        values["contact"] = contactPerson?.text! as AnyObject
        values["description"] = desc?.text! as AnyObject
        
        if !phoneNumber!.isNilOrEmpty() {
            values["phone"] = phoneNumber?.text! as AnyObject
        }
        
        if !website!.isNilOrEmpty() {
            values["website"] = website?.text! as AnyObject
        }
        
        if !facebookURL!.isNilOrEmpty() {
            values["facebook"] = facebookURL?.text! as AnyObject
        }
        
        if !twitterURL!.isNilOrEmpty() {
            values["twitter"] = twitterURL?.text! as AnyObject
        }
        
        if !email!.isNilOrEmpty() {
            values["email"] = email?.text! as AnyObject
        }
        
        if !stAddress1!.isNilOrEmpty(), !city!.isNilOrEmpty(), !state!.isNilOrEmpty(), !zip!.isNilOrEmpty() {
            locationValues["streetAddress1"] = stAddress1!.text! as AnyObject
            if !stAddress2!.isNilOrEmpty() {
                locationValues["streetAddress2"] = stAddress2!.text! as AnyObject
            }
            
            if !stAddress3!.isNilOrEmpty() {
                locationValues["streetAddress3"] = stAddress3!.text! as AnyObject
            }
            locationValues["city"] = city!.text! as AnyObject
            locationValues["state"] = state!.text! as AnyObject
            locationValues["zip"] = zip!.text! as AnyObject
            
            let address = Location.makeAddress(ofType: .Full, streetAddress1: stAddress1!.text!, streetAddress2: stAddress2!.text, streetAddress3: stAddress3!.text, city: city!.text!, state: state!.text!, zip: zip!.text!)
            let geoCoder = CLGeocoder()
            geoCoder.geocodeAddressString(address) { (placemarks, error) in
                if placemarks != nil {
                    if let location = placemarks!.first!.location {
                        let latitude = String(location.coordinate.latitude as Double)
                        let longitude = String(location.coordinate.longitude as Double)
                        locationValues["latitude"] = latitude as AnyObject
                        locationValues["longitude"] = longitude as AnyObject
                    }
                }
                values["location"] = locationValues as AnyObject
                self.finishRefreshResource(values: values)
                return
            }
        } else {
            finishRefreshResource(values: values)
            return
        }
    }
    
    func finishRefreshResource(values: [String: AnyObject]) {
        let ref = Database.database().reference().child(group.key)
        
        ref.updateChildValues(values, withCompletionBlock: { error, _ in
            guard error == nil else {
                self.Filter(disable: true)
                SCLAlertView().showError("Error", subTitle: "An error occured, please try again")
                return
            }
            self.Filter(disable: true)
            _ = self.navigationController?.popViewController(animated: true)
        })
    }
    
    func infoPressed(sender: UITapGestureRecognizer) {
        var title = sender.accessibilityLabel!
        if title.range(of: " (Optional)") != nil {
            title = title.replacingOccurrences(of: " (Optional)", with: "")
        } else if title.range(of: " (Required for location)") != nil {
            title = title.replacingOccurrences(of: " (Required for location)", with: "")
        }
        switch title {
        case "Resource Name":
            postHelp(title: title, subtitle: ResourceHelp.name.rawValue)
        case "Contact Person":
            postHelp(title: title, subtitle: ResourceHelp.contact_person.rawValue)
        case "Phone Number":
            postHelp(title: title, subtitle: ResourceHelp.phone_number.rawValue)
        case "Website":
            postHelp(title: title, subtitle: ResourceHelp.website.rawValue)
        case "Facebook ID":
            postHelp(title: title, subtitle: ResourceHelp.facebook_id.rawValue)
        case "Twitter Handle":
            postHelp(title: title, subtitle: ResourceHelp.twitter_handle.rawValue)
        case "Description":
            postHelp(title: title, subtitle: ResourceHelp.description.rawValue)
        case "Email":
            postHelp(title: title, subtitle: ResourceHelp.email.rawValue)
        case "Street Address 1":
            postHelp(title: title, subtitle: ResourceHelp.street_address_1.rawValue)
        case "Street Address 2":
            postHelp(title: title, subtitle: ResourceHelp.street_address_2.rawValue)
        case "Street Address 3":
            postHelp(title: title, subtitle: ResourceHelp.street_address_3.rawValue)
        case "City":
            postHelp(title: title, subtitle: ResourceHelp.city.rawValue)
        case "State":
            postHelp(title: title, subtitle: ResourceHelp.state.rawValue)
        case "Zip Code":
            postHelp(title: title, subtitle: ResourceHelp.zip.rawValue)
        default: break
        }
    }
    
    func postHelp(title: String, subtitle: String) {
        SCLAlertView().showInfo(title, subTitle: subtitle)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func greyOut(enable: Bool) {
        if enable {
            publish.setTitleColor(UIColor.gray, for: .normal)
            publish.isEnabled = false
        } else {
            publish.setTitleColor(UIColor(red: 23, green: 70, blue: 186), for: .normal)
            publish.isEnabled = true
        }
    }
    
    func keyboardWasShown(notification: NSNotification) {
        scrollView.contentSize.height = 840
    }
    
    func keyboardWasHidden(notification: NSNotification) {
        scrollView.contentSize.height = self.view.frame.size.height
    }
}
