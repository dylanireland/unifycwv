//
//  FullEventViewController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 8/6/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import EventKit
import SCLAlertView
import FirebaseAuth
import FirebaseDatabase

class FullEventViewController: UIViewController {
    
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    
    @IBOutlet weak var directionsButton: UIButton!
    @IBOutlet weak var addToCalendar: UIButton!
    
    @IBOutlet weak var verifyButton: UIButton!
    
    var event: Event!
    var groupName: String!
    
    var editEventButton = UIBarButtonItem()
    var deleteEventButton = UIBarButtonItem()
    
    internal var admin = false
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        editEventButton = UIBarButtonItem(image: #imageLiteral(resourceName: "edit"), style: .plain, target: self, action: #selector(barButtonPressed(sender:)))
        editEventButton.accessibilityLabel = "edit"
        
        deleteEventButton = UIBarButtonItem(image: #imageLiteral(resourceName: "trash"), style: .plain, target: self, action: #selector(barButtonPressed(sender:)))
        deleteEventButton.accessibilityLabel = "delete"
        navigationItem.setRightBarButtonItems([editEventButton, deleteEventButton], animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if admin {
            verifyButton.titleLabel?.textAlignment = .center
            verifyButton.setTitle("Verify", for: .normal)
            verifyButton.titleLabel?.font = UIFont().Default(withSize: 18)
            verifyButton.setTitleColor(UIColor.Default, for: .normal)
            verifyButton.isUserInteractionEnabled = true
            verifyButton.isHidden = false
            verifyButton.isEnabled = true
            self.view.bringSubview(toFront: verifyButton)
        } else {
            verifyButton.isHidden = true
            verifyButton.isEnabled = false
            verifyButton.isUserInteractionEnabled = false
        }
        
        groupNameLabel.text = groupName
        startLabel.text = "Starts: \(event.startTime.makeStringFromHHmm()) | \(event.date.makeStringFromYMD())"
        endLabel.text = "Ends: \(event.endTime.makeStringFromHHmm()) | \(event.endDate.makeStringFromYMD())"
        eventNameLabel.text = event.name
        descriptionLabel.text = event.desc
        locationLabel.text = event.location?.makeAddress(ofType: .Display) ?? "Location Unknown"
        if event.locationName == nil, event.location == nil {
            directionsButton.isEnabled = false
        } else {
            directionsButton.isEnabled = true
        }
        
        barButtonAvailability()
    }
    
    @IBAction func Verify() {
        guard admin else {
            return
        }
        
        Filter(disable: false)
        
        let values: [AnyHashable: Any] = ["events/\(event.key)/valid": true, "group-events/\(event.groupId)/\(event.key)/valid": true]
        
        Database.database().reference().updateChildValues(values) { error, _ in
            self.Filter(disable: true)
            if error != nil {
                SCLAlertView().showError("Error", subTitle: "Failed to verify event")
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func directions() {
        if let address = event.locationName, let encodedName = address.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            let baseUrl: String = "http://maps.apple.com/?q="
            let finalUrl = baseUrl + encodedName
            if let url = URL(string: finalUrl) {
                UIApplication.shared.openURL(url)
            }
        }
        guard let address = event.location?.makeAddress(ofType: .Full), let encodedName = address.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            return
        }
        let baseUrl: String = "http://maps.apple.com/?q="
        let finalUrl = baseUrl + encodedName
        if let url = URL(string: finalUrl) {
            UIApplication.shared.openURL(url)
        }
    }
    
    func barButtonAvailability() {
        guard let groups = Group.get(), let firebaseUser = Auth.auth().currentUser else {
            editEventButton.tintColor = UIColor.clear
            editEventButton.isEnabled = false
            deleteEventButton.tintColor = UIColor.clear
            deleteEventButton.isEnabled = false
            return
        }
        
        var checkableGroup: Group?
        for group in groups {
            if group.key == event.groupId {
                checkableGroup = group
                break
            }
        }
        
        guard let group = checkableGroup else {
            return
        }
        
        var users: [Key] = [Key]()
        users.append(group.owner)
        if let addableUsers = group.users {
            for user in addableUsers {
                users.append(user.key)
            }
        }
        
        if users.contains(firebaseUser.uid) || admin {
            editEventButton.tintColor = UIColor(red: 23, green: 70, blue: 186)
            editEventButton.isEnabled = true
            deleteEventButton.tintColor = UIColor(red: 23, green: 70, blue: 186)
            deleteEventButton.isEnabled = true
        }
    }
    
    func barButtonPressed(sender: UIBarButtonItem) {
        if sender.accessibilityLabel == "edit" {
            if let EditEventVC = storyboard?.instantiateViewController(withIdentifier: "EditEventController") as? EditEventController {
                EditEventVC.event = event
                navigationController?.pushViewController(EditEventVC, animated: true)
            }
        } else if sender.accessibilityLabel == "delete" {
            preAskForDelete()
        }
    }
    
    func preAskForDelete() {
        let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("Yes") {
            self.deleteEvent()
        }
        alertView.addButton("No", action: {})
        alertView.showInfo("Delete", subTitle: "Are you sure you want to delete this event?")
    }
    
    func deleteEvent() {
        let values: [AnyHashable: Any] = ["events/\(event.key)": NSNull(), "group-events/\(event.groupId)/\(event.key)": NSNull(), "groups/\(event.groupId)/events/\(event.key)": NSNull()]
        Database.database().reference().updateChildValues(values, withCompletionBlock: { error, _ in
            if error != nil {
                SCLAlertView().showError("Error", subTitle: "An error occured while deleting this event, please try again")
            } else {
                SCLAlertView().showSuccess("Success", subTitle: "Your event has been successfully deleted")
                _ = self.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    @IBAction func addToCal() {
        guard let start = event.date.combineWith(time: event.startTime), let end = event.endDate.combineWith(time: event.endTime) else {
            return
        }
        addEventToCalendar(title: event.name, description: event.desc, startDate: start, endDate: end, completion: { success, error in
            var alert = UIAlertController()
            if success {
                alert = UIAlertController(title: "You're All Set", message: "\"\(self.event.name)\" was successfully added to your calender", preferredStyle: UIAlertControllerStyle.alert)
            } else {
                if let error = error {
                    alert = UIAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                } else {
                    alert = UIAlertController(title: "Whoops!", message: "Something went wrong, please try again", preferredStyle: UIAlertControllerStyle.alert)
                }
            }
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                event.location = self.event.locationName ?? self.event.location?.makeAddress(ofType: .Full)
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let error as NSError {
                    completion?(false, error)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
}
