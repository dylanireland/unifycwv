//
//  HomeController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 9/10/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import SCLAlertView

class HomeController: UIViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.post(name: NSNotification.Name("tabBarViewChanged"), object: nil, userInfo: ["key": 0])
    }
}
