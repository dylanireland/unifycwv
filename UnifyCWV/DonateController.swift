//
//  DonateController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 8/14/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import PassKit

class DonateController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var donationAmountPicker: UIPickerView!
    var donationAmountPickerData: [String] = [String]()
    var selectedAmount = Int()
    let amountKey: [Int: Int] = [0: 2, 1: 10, 2: 40, 3: 120]
    //Be sure to change if you have different picker values, measured in [picker row] to [USD]
    
    @IBOutlet weak var customAmountField: UITextField!
    @IBOutlet weak var 🤑: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Donate"
        
        customAmountField.isHidden = true
        🤑.isHidden = true
        
        self.donationAmountPicker.delegate = self
        self.donationAmountPicker.dataSource = self
        
        customAmountField.delegate = self
        donationAmountPickerData = ["A cup of coffee ($2)", "A week's maintenance ($10)", "A month's maintenance ($40)", "A fiscal quarter's maintenance ($120)", "Custom"]
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont().Default(withSize: label.font.pointSize)
        
        // where data is an Array of String
        label.text = donationAmountPickerData[row]
        
        return label
    }
    
    @IBAction func donate() {
        var amount = Int()
        if selectedAmount == donationAmountPickerData.count - 1 {
            //This is another example of the ....count - 1 comparison listed below
            if customAmountField.text == "" || customAmountField.text == nil {
                return
            }
            if Int(customAmountField.text!)! == 0 {
                return
            }
            amount = Int(customAmountField.text!)!
        } else {
            amount = amountKey[selectedAmount]!
        }
        print("You have chosen to donate $\(amount), is this correct?")
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return donationAmountPickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return donationAmountPickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedAmount = row
        if row == donationAmountPickerData.count - 1 /*Custom*/ {
            customAmountField.isHidden = false
            🤑.isHidden = false
            /*if statement says "if selected row is equal to the total amount of values in the picker data array (minus 1, because row starts from 0 but picker data count starts at 1), execute."
                I took this approach as opposed to "if donationAmountPickerData[row] == "Custom"" because this approach allows you to change the value of "Custom" without breaking the code (so long as the last value of the array is the custom one). */
        } else {
            customAmountField.isHidden = true
            🤑.isHidden = true
            customAmountField.text = "1"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
