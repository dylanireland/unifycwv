//
//  MyInfoController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 8/22/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import SCLAlertView

class MyInfoController: UIViewController {
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    
    @IBOutlet weak var refreshEmailVerificationStatus: UIButton!
    @IBOutlet weak var finishAccountSetupButton: UIButton!
    @IBOutlet weak var finishAccountSetupTextView: UITextView!
    
    @IBOutlet weak var nav2: UINavigationItem!
    @IBOutlet weak var navBar: UINavigationBar!
    
    var oldUserValues: [String: String] = [:]
    
    var bottomY: CGFloat = 0
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        navBar.titleTextAttributes = [NSFontAttributeName: UIFont().Default(withSize: 18), NSForegroundColorAttributeName: UIColor.white]
        
        nav2.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont().Default(withSize: 15)], for: .normal)
        
        nav2.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont().Default(withSize: 15)], for: .normal)
        
        emailField.isUserInteractionEnabled = false
        
        userInteractionFields(disable: true)
        
        if let email = Auth.auth().currentUser?.email {
            emailField.isHidden = false
            emailField.text = email
            usernameField.isHidden = false
            let username = email.components(separatedBy: "@")[0]
            usernameField.text = username
        } else {
            usernameField.isHidden = true
            emailField.isHidden = true
        }
        
        nav2.rightBarButtonItem?.target = self
        nav2.rightBarButtonItem?.action = #selector(edit)
        
        nav2.leftBarButtonItem?.target = self
        nav2.leftBarButtonItem?.action = #selector(done)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        finishedSignUp()
        loadFinishOrVerifyViews()
        postUserPartOfGroups()
        checkAdmin()
    }
    
    func getBottomY() -> CGFloat {
        if !phoneNumberField.isHidden {
            return phoneNumberField.frame.maxY
        } else if !nameField.isHidden && phoneNumberField.isHidden {
            return nameField.frame.maxY
        } else {
            return emailField.frame.maxY
        }
    }
    
    func listOutGroups(partOf groups: [String], askedOf requested: [String]) {
        bottomY = getBottomY() + 10
        enumerate(over: groups, requested: false)
        enumerate(over: requested, requested: true)
    }
    
    func enumerate(over groups: [String], requested: Bool) {
        if groups.count != 0 {
            let resourcesLabelFrame = CGRect(x: self.view.frame.midX - 125, y: bottomY, width: 250, height: 30)
            bottomY += 30
            let resourcesLabel = UILabel(frame: resourcesLabelFrame)
            resourcesLabel.font = UIFont().Default(withSize: 16)
            if requested {
                resourcesLabel.text = "Resources you've requested:"
            } else {
                resourcesLabel.text = "Resources you're a part of:"
            }
            resourcesLabel.textColor = UIColor.darkGray
            resourcesLabel.textAlignment = .center
            self.view.addSubview(resourcesLabel)
            
            
            for (index, group) in groups.enumerated() {
                let label = UILabel()
                var x: CGFloat {
                    if index % 2 == 0 {
                        label.textAlignment = .right
                        return self.view.frame.midX - 210
                    } else {
                        label.textAlignment = .left
                        return self.view.frame.midX + 10
                    }
                }
                var only_x: CGFloat?
                if group == groups.last {
                    label.textAlignment = .center
                    only_x = self.view.frame.midX - 100
                }
                let frame = CGRect(origin: CGPoint(x: only_x ?? x, y: bottomY), size: CGSize(width: 200, height: 30))
                if index % 2 != 0 {
                    bottomY += 30
                }
                label.frame = frame
                label.font = UIFont().Default(withSize: 15)
                label.textColor = UIColor.darkGray
                label.text = group
                self.view.addSubview(label)
            }
            if !requested {
                bottomY += 30
            }
        }
    }
    
    func postUserPartOfGroups() {
        if let groups = User.getCurrentUser()?.groups, let requests = User.getCurrentUser()?.requests {
            var groupList: [String] = []
            var requestList: [String] = []
            for group in groups {
                groupList.append(group.value)
            }
            for request in requests {
                requestList.append(request.value)
            }
            listOutGroups(partOf: groupList, askedOf: requestList)
        }
    }
    
    func loadFinishOrVerifyViews() {
        nav2.rightBarButtonItem?.tintColor = UIColor.clear
        nav2.rightBarButtonItem?.isEnabled = false
        if !Auth.auth().currentUser!.isEmailVerified {
            finishAccountSetupButton.isHidden = false
            finishAccountSetupTextView.isHidden = false
            finishAccountSetupButton.setTitle("Verify Email", for: .normal)
            finishAccountSetupTextView.text = "Please verify your email address to continue setting up your account"
            refreshEmailVerificationStatus.isHidden = false
            nameField.isHidden = true
            phoneNumberField.isHidden = true
            return
        } else if let complete = User.getCurrentUser()?.complete {
            if complete {
                finishAccountSetupButton.isHidden = true
                finishAccountSetupTextView.isHidden = true
                refreshEmailVerificationStatus.isHidden = true
                nav2.rightBarButtonItem?.tintColor = UIColor.white
                nav2.rightBarButtonItem?.isEnabled = true
                nameField.isHidden = false
                phoneNumberField.isHidden = false
                return
            }
        }
        finishAccountSetupButton.isHidden = false
        finishAccountSetupTextView.isHidden = false
        finishAccountSetupButton.setTitle("Finish Account Setup", for: .normal)
        finishAccountSetupTextView.text = "You have not finished setting up your account, please do so below to unlock new features"
        refreshEmailVerificationStatus.isHidden = true
        nameField.isHidden = true
        phoneNumberField.isHidden = true
    }
    
    func finishedSignUp() {
        if let name = User.getCurrentUser()?.name {
            nameField.isHidden = false
            nameField.text = name
        } else {
            nameField.isHidden = true
        }
        if let phone = User.getCurrentUser()?.phone {
            phoneNumberField.isHidden = false
            phoneNumberField.text = phone
        } else {
            phoneNumberField.isHidden = true
        }
    }
    
    func checkAdmin() {
        guard User.getCurrentUser()?.admin == true else {
            return
        }
        let labelHeight: CGFloat = 30
        let label = UILabel(frame: CGRect(x: self.view.frame.midX - (self.view.frame.width / 2), y: self.view.frame.maxY - (labelHeight + 5), width: self.view.frame.width, height: labelHeight))
        label.font = UIFont().Default(withSize: 15)
        label.textColor = UIColor.gray
        label.textAlignment = .center
        label.text = "You are an administrator"
        self.view.addSubview(label)
    }
    
    func edit() {
        oldUserValues["name"] = nameField.text!
        oldUserValues["phone"] = phoneNumberField.text!
        userInteractionFields(disable: false)
        nameField.becomeFirstResponder()
    }
    
    func done() {
        if nameField.isUserInteractionEnabled || phoneNumberField.isUserInteractionEnabled {
            var name: String? = nil
            var phone: String? = nil
            if nameField.text == nil || nameField.text == "" || phoneNumberField.text == nil || phoneNumberField.text == "" {
                if nameField.text == nil || nameField.text == "" {
                    nameField.text = oldUserValues["name"]
                }
                if phoneNumberField.text == nil || phoneNumberField.text == "" {
                    phoneNumberField.text = oldUserValues["phone"]
                }
            }
            if nameField.text != oldUserValues["name"] {
                name = nameField.text
            }
            if phoneNumberField.text != oldUserValues["phone"] {
                phone = phoneNumberField.text
            }
            self.view.endEditing(true)
            userInteractionFields(disable: true)
            if name == nil && phone == nil {
                return
            }
            updateUserInfo(name: name, phone: phone)
        } else {
            self.performSegue(withIdentifier: "dismissMyInfo", sender: self)
        }
    }
    
    func updateUserInfo(name: String?, phone: String?) {
        if let user = Auth.auth().currentUser {
            var values: [String: String] = [:]
            if name != nil {
                values["name"] = name!
            }
            if phone != nil {
                values["phone"] = phone!
            }
            Filter(disable: false)
            self.ref.child("users").child(user.uid).updateChildValues(values, withCompletionBlock: { error, _ in
                guard error == nil else {
                    self.throwError()
                    return
                }
                User.getCurrentUser()?.update(name: name, phone: phone)
                self.oldUserValues = [:]
                self.Filter(disable: true)
                self.view.endEditing(true)
            })
        } else {
            throwError()
        }
    }
    
    func throwError() {
        nameField.text = oldUserValues["name"]
        phoneNumberField.text = oldUserValues["phone"]
        Filter(disable: true)
        SCLAlertView().showError("Error", subTitle: "An error occured, please try again")
    }

    @IBAction func finishAccountOrVerify() {
        if !Auth.auth().currentUser!.isEmailVerified {
            Filter(disable: false)
            Auth.auth().currentUser!.sendEmailVerification(completion: { error in
                guard error == nil else {
                    self.Filter(disable: true)
                    SCLAlertView().showError("Error", subTitle: "An error occured, please try again")
                    return
                }
                self.Filter(disable: true)
                SCLAlertView().showInfo("Info", subTitle: "A verification email has been sent to \(Auth.auth().currentUser?.email ?? "your email address")")
            })
        } else if let complete = User.getCurrentUser()?.complete {
            if !complete {
                self.performSegue(withIdentifier: "finishAccount", sender: self)
            }
        } else {
            self.performSegue(withIdentifier: "finishAccount", sender: self)
        }
    }
    
    @IBAction func refreshEmailVerification() {
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        let field = alertView.addTextField("Password")
        field.isSecureTextEntry = true
        alertView.addButton("Refresh") {
            SCLAlertView().dismiss(animated: true)
            self.Filter(disable: false)
            Auth.auth().signIn(withEmail: Auth.auth().currentUser!.email!, password: field.text!, completion: { _, error in
                self.Filter(disable: true)
                guard error == nil else {
                    SCLAlertView().showError("Error", subTitle: "An error occured, please try again")
                    return
                }
                self.loadFinishOrVerifyViews()
            })
        }
        alertView.addButton("Cancel", action: {})
        alertView.showInfo("One sec", subTitle: "Please enter your password")
    }
    
    func userInteractionFields(disable: Bool) {
        if disable {
            nameField.isUserInteractionEnabled = false
            phoneNumberField.isUserInteractionEnabled = false
        } else {
            nameField.isUserInteractionEnabled = true
            phoneNumberField.isUserInteractionEnabled = true
        }
    }
}
