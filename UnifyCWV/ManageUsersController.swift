//
//  ManageUsersController.swift
//  
//
//  Created by Dylan Ireland on 10/30/17.
//
//

import UIKit
import SCLAlertView
import FirebaseDatabase

class ManageUsersController: UITableViewController {
    
    var group: Group!
    var users: [Key: Name] = [:]

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let requests = GroupRequest.get() {
            for request in requests {
                if request.groupID == group.key {
                    users[request.userID] = request.name
                }
            }
        }
        
        if users.isEmpty {
            SCLAlertView().showInfo("No Users", subTitle: "There are no users that have requested to join at this time")
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let requesterLabel = cell.viewWithTag(10) as! UILabel
        requesterLabel.text = Array(users)[indexPath.row].value
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let key = Array(users)[row].key
        let name = Array(users)[row].value
        
        preAskAllow(with: key, for: name, at: row)
    }
    
    func preAskAllow(with key: String, for name: String, at row: Int) {
        let appearance = SCLAlertView.SCLAppearance(showCloseButton: true)
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("Yes", action: {
            self.performAllowance(allowed: true, row: row)
            return
        })
        alertView.addButton("No", action: {
            self.performAllowance(allowed: false, row: row)
            return
        })
        alertView.showInfo("Accept", subTitle: "Would you like to allow \(name) to join \(group.name)")
    }
    
    func performAllowance(allowed: Bool, row: Int) {
        let key = Array(users)[row].key
        let name = Array(users)[row].value
        
        Filter(disable: false)
        let reference = Database.database().reference()
        var values: [AnyHashable: Any] = ["group-requests/\(group.key)/\(key)": NSNull()]
        if allowed {
            values["groups/\(group.key)/users/\(key)"] = name
        }
        reference.updateChildValues(values, withCompletionBlock: { error, _ in
            self.Filter(disable: true)
            guard error == nil else {
                SCLAlertView().showError("Uh oh", subTitle: "An error occured, please try again")
                return
            }
            self.refreshTableFromRemoval(removing: row)
            SCLAlertView().showSuccess("Success", subTitle: "\(name) was added to the \(self.group.name)")
        })
    }
    
    func refreshTableFromRemoval(removing row: Int) {
        let key = Array(users)[row].key
        users.removeValue(forKey: key)
        self.tableView.reloadData()
    }
}
