//
//  EventListController.swift
//  UnifyCWV
//
//  Created by Dylan Ireland on 3/3/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import SwiftyJSON

class EventListController: UITableViewController, UISearchBarDelegate {

    var eventList: [Event]? = nil
    
    var segueIdentifier: String?
    var keys = [Key]()
    var searchBar: UISearchBar!
    
    private var admin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(incomeNewEvents), name: NSNotification.Name("incomeNewData"), object: nil)
        initSearchBar()
    }
    
    func initSearchBar() {
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 70))
        searchBar.showsScopeBar = true
        searchBar.scopeButtonTitles = ["Date", "Resource", "Name"]
        searchBar.delegate = self
        self.tableView.tableHeaderView = searchBar
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let storyboardID = self.restorationIdentifier {
            if storyboardID == "AdminEventListController" {
                admin = true
                navigationController?.navigationBar.isHidden = true
            }
        }
        NotificationCenter.default.post(name: NSNotification.Name("tabBarViewChanged"), object: nil, userInfo: ["key": 1])
        incomeNewEvents()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if admin {
            navigationController?.navigationBar.isHidden = false
        }
    }
    
    func incomeNewEvents() {
        eventList = Event.get()
        rearrangeForAdmin()
        rearrangeForCalendar()
        switch searchBar.selectedScopeButtonIndex {
        case 0:
            sort(by: .Date)
        case 1:
            sort(by: .Resource)
        case 2:
            sort(by: .Name)
        default:
            break
        }
        self.tableView.reloadData()
    }
    
    func rearrangeForAdmin() {
        if eventList != nil, eventList!.count != 0, admin {
            var newList = [Event]()
            for index in 0..<eventList!.count {
                let event = eventList![index]
                if !event.valid {
                    let element = eventList!.remove(at: index)
                    eventList!.insert(element, at: index)
                    newList.append(element)
                }
            }
            eventList = newList //Keys will for certain be empty
        }
    }
    
    func rearrangeForCalendar() {
        if !keys.isEmpty {
            var newList = [Event]()
            for event in eventList! {
                for key in keys {
                    if event.key == key {
                        newList.append(event)
                    }
                }
            }
            eventList = newList
        }
    }
    
    func sorterChanged() {
        switch searchBar.selectedScopeButtonIndex {
        case SorterIndices.Date.hashValue:
            sort(by: SorterIndices.Date)
        case SorterIndices.Resource.hashValue:
            sort(by: SorterIndices.Resource)
        case SorterIndices.Name.hashValue:
            sort(by: SorterIndices.Name)
        default: break
        }
    }
    
    enum SorterIndices {
        case Date, Resource, Name
    }
    
    func sort(by index: SorterIndices) {
        guard eventList != nil else {
            return
        }
        if index == .Date {
            eventList!.sort { $0.date.combineWith(time: $0.startTime)! < $1.date.combineWith(time: $1.startTime)! }
        }
        if index == .Resource {
            eventList!.sort { $0.groupName ?? "ZZZZ" < $1.groupName ?? "ZZZZ" }
        }
        if index == .Name {
            eventList!.sort { $0.name < $1.name }
        }
        descendInvalidEvents()
        self.tableView.reloadData()
    }
    
    func descendInvalidEvents() {
        for index in 0..<eventList!.count {
            let event = eventList![index]
            if !event.valid || event.groupName == nil {
                let element = eventList!.remove(at: index)
                eventList!.insert(element, at: eventList!.endIndex)
            }
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if eventList == nil {
            return 0
        } else {
            return eventList!.count
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let eventList = eventList, let groupName = eventList[indexPath.row].groupName, let fullEventListVC = storyboard?.instantiateViewController(withIdentifier: "FullEventViewController") as? FullEventViewController else {
            return
        }
        fullEventListVC.event = eventList[indexPath.row]
        fullEventListVC.groupName = groupName
        fullEventListVC.admin = admin
        navigationController?.pushViewController(fullEventListVC, animated: true)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        guard eventList != nil, eventList!.count != 0 else {
            return cell
        }
        
        let event = eventList![indexPath.row]
        
        if !event.valid {
            cell.backgroundColor = UIColor.lightGray
            cell.isUserInteractionEnabled = false
        } else {
            cell.backgroundColor = UIColor.white
            cell.isUserInteractionEnabled = true
        }
        if admin {
            cell.isUserInteractionEnabled = true
            cell.backgroundColor = UIColor.white
        }
        
        let groupLabel = cell.viewWithTag(12) as! UILabel
        groupLabel.text = event.groupName ?? "Resource Name Unknown"
        
        let dateLabel = cell.viewWithTag(10) as! UILabel
        let startTime = event.startTime
        let date = event.date
        
        dateLabel.text = "\(startTime.makeStringFromHHmm().toTwelve()) | \(date.makeStringFromYMD())"
        
        let nameLabel = cell.viewWithTag(11) as! UILabel
        
        nameLabel.text = event.name
        
        let locationLabel = cell.viewWithTag(13) as! UILabel
        if let locationName = event.locationName {
            locationLabel.text = locationName
        } else {
            locationLabel.text = event.location?.makeAddress(ofType: .Display) ?? "Location Unknown"
        }
        
        return cell
    }
    
    // MARK: UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            incomeNewEvents()
        }
        
        filterTableView(from: searchText.lowercased())
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        sorterChanged()
    }
    
    func filterTableView(from text: String) {
        guard eventList != nil else {
            return
        }
        switch searchBar.selectedScopeButtonIndex {
        case 0:
            _ = eventList!.filter({ event -> Bool in
                return event.date.provideSearchableText().lowercased().contains(text)
            })
        case 1:
            _ = eventList!.filter({ event -> Bool in
                return event.groupName?.lowercased().contains(text) ?? false
            })
        case 2:
            _ = eventList!.filter({ event -> Bool in
                return event.name.lowercased().contains(text)
            })
        default:
            break
        }
        self.tableView.reloadData()
    }
    
}
