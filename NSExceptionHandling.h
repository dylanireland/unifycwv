//
//  NSExceptionHandlingHeader.h
//  UnifyCWV
//
//  Created by Dylan Ireland on 12/24/17.
//  Copyright © 2017 Dylan Ireland. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSExceptionHandling: NSObject

+ (BOOL)catchException:(void(^)())tryBlock error:(__autoreleasing NSError **)error;

@end
